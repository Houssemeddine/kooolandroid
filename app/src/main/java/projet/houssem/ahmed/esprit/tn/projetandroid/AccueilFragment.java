package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.PopulairePastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AccueilFragment extends Fragment {
    RequestQueue queue ;
    ListView listView;
    Button btnplusPastry;
    Button btnconnection;
    Button BtnProximity;
    Button recament;
    Button BtnDernier;
    String urlPastry = Ressource.url+"/api/tpastries";
    String urlPhotos =Ressource.url+"/api/tphotos";
    String urlgetPopulairePastry = Ressource.url+"/getPopulairePastry";
    String urlNewPastry = Ressource.url+"/getNewPastry";
     pastryAcueilAdapter adapter;
    List<Pastry> pastrys;
    EditText searchtext;
    SwipeRefreshLayout refresh;

    //sharedpreference pour le button conn/decon
    SharedPreferences pref ;// 0 - for private mode

    SharedPreferences.Editor editor;

    //recycleview pour l'affichage horizentalenmet

    LinearLayoutManager layoutManager;
    RecyclerView myList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_accueil, container, false);
        btnconnection=(Button)root.findViewById(R.id.btnconnection);
        BtnProximity=(Button)root.findViewById(R.id.BtnProximity);
        BtnDernier=(Button)root.findViewById(R.id.Btndernier);
        refresh=(SwipeRefreshLayout)root.findViewById(R.id.refresh);
        BtnDernier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });
        BtnProximity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ProximityPastryActivity.class);

                startActivity(intent);
            }
        });

        recament =(Button)root.findViewById(R.id.Btndernier);
        recament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),RecemmentVisiteActivity.class);
                startActivity(intent);
            }
        });
//searchtext.setSelected(false);

  searchtext=(EditText)root.findViewById(R.id.search);
searchtext.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(),AllPastryActivity.class);

        startActivity(intent);

    }
});
        listView = (ListView)root.findViewById(R.id.listPastry);
        btnplusPastry=(Button)root.findViewById(R.id.btnplus) ;

        pref = getApplicationContext().getSharedPreferences("mypref", Context.MODE_PRIVATE); // 0 - for private mode

        btnconnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getBoolean("connexion",false)){
                    Intent intent = new Intent(getActivity(),LoginActivity.class);

                    startActivity(intent);

                }else {
                    editor = pref.edit();
                    editor.putBoolean("connexion",false);
                    editor.commit();
                    Intent intent = new Intent(getActivity(),HomeActivity.class);

                    startActivity(intent);

                }
            }
        });
        btnplusPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AllPastryActivity.class);

                startActivity(intent);

            }
        });
//horizental affiche
         layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

         myList = (RecyclerView) root.findViewById(R.id.framehoriz);
        myList.setLayoutManager(layoutManager);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewPastry();
                getPopulairePastry();
                refresh.setRefreshing(false);
            }
        });
        getNewPastry();
        getPopulairePastry();



        return root;

    }


    public void getPastry(){
        queue = Volley.newRequestQueue(getActivity());

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Pastry>>() {
                        }.getType();
                       pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                         adapter = new pastryAcueilAdapter(  getApplicationContext(), R.layout.item_acueil, pastrys);

                        listView.setAdapter(adapter);
                        //ArrayAdapter<Pastry> adaprecyle =new pastryAcueilAdapter(getApplicationContext(),R.layout.item_acueil,pastrys);


                        recycleAccueilPastryAdapter horizentallistAdapter=new recycleAccueilPastryAdapter(getActivity(),pastrys);
                        myList.setAdapter(horizentallistAdapter);
                        adapter.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }

    public void getNewPastry(){
        queue = Volley.newRequestQueue(getActivity());

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlNewPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                       try {
                           Log.d("Response", response.toString());
                           JSONObject jsonobject = null;
                           Type listType = new TypeToken<ArrayList<Pastry>>() {
                           }.getType();
                           List<Pastry> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                           System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                           if (pastrys!=null) {
                               recycleAccueilPastryAdapter horizentallistAdapter =
                                       new recycleAccueilPastryAdapter(getActivity(), pastrys);
                               myList.setAdapter(horizentallistAdapter);

                           }
                       }catch (Exception e){
                           getNewPastry();
                       }




                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                      //  Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }
    public void getPopulairePastry(){
        queue = Volley.newRequestQueue(getActivity());

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlgetPopulairePastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<PopulairePastry>>() {
                        }.getType();
                        List<PopulairePastry> populairePastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt"+populairePastrys);
                        List<Pastry> pastrys= new ArrayList<>();
                        for (PopulairePastry populairePastry1:populairePastrys) {
                            pastrys.add(populairePastry1.getPastry());
                        }
                        if (pastrys!=null) {
                            pastryAcueilAdapter adapter = new pastryAcueilAdapter(getActivity(), R.layout.item_acueil, pastrys);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                       // Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }

}
