package projet.houssem.ahmed.esprit.tn.projetandroid;

        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ProgressBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.RetryPolicy;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.HashMap;
        import java.util.Map;

        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class EditProfileActivity extends AppCompatActivity {
    EditText nom;
    EditText tel;
    EditText email;
   // EditText password;
   // EditText repassword;
    Button btn;
    Button btnModifierPassword;
    //Button btnf;
    TextView Error;

    //préference
    SharedPreferences pref ;// 0 - for private mode
    private ProgressBar spinner;


    User user=new User();
    RequestQueue queue ;
    String url = Ressource.url+"/api/tusers/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);

        nom=(EditText)findViewById(R.id.nom);
        tel=(EditText)findViewById(R.id.tel);
        email=(EditText)findViewById(R.id.email);
        //password=(EditText)findViewById(R.id.password);
       // repassword=(EditText)findViewById(R.id.repassword);
        btn=(Button) findViewById(R.id.btnregister);
        btnModifierPassword=(Button) findViewById(R.id.btnModifierPassword);
       // btnf=(Button) findViewById(R.id.btnf);
        // Error=(TextView) findViewById(R.id.Error);
        btn.setText("modifier");
       // btnf.setVisibility(View.GONE);
        //password.setVisibility(View.GONE);
        //repassword.setVisibility(View.GONE);
        nom.setText(User.userConnect.getNom().toString());
        email.setText(User.userConnect.getEmail().toString());
        tel.setText(User.userConnect.getTel()+"");
       // password.setText(User.userConnect.getPassword().toString());
       // repassword.setText(User.userConnect.getPassword().toString());
        queue = Volley.newRequestQueue(this);
        btnModifierPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProfileActivity.this,EditPasswordActivity.class);

                startActivity(intent);

            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setVisibility(View.VISIBLE);
                btn.setEnabled(false);
                btnModifierPassword.setEnabled(false);
                if (verifCHampEmty()) {
                    if (true) {
                        user.setTel((int) Integer.parseInt(tel.getText().toString()));
                        user.setEmail(email.getText().toString());
                        user.setPassword(User.userConnect.getPassword());
                        user.setNom(nom.getText().toString());

                        // Request a string response from the provided URL.


                        StringRequest postRequest = new StringRequest(Request.Method.PUT, url+User.userConnect.getId(),
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // response
                                        Log.d("Response", response.toString());

                                        JSONObject jsonobject = null;
                                        try {
                                            jsonobject = new JSONObject(response);
                                            User.userConnect.setTel(jsonobject.getInt("tel"));
                                            User.userConnect.setEmail(jsonobject.getString("email"));
                                            User.userConnect.setNom(jsonobject.getString("nom"));
                                            // authPreference(true);
                                            SucessInscription();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            System.out.println("error dans la response jsonObject");
                                            tostMessage("Error de saisie");
                                            spinner.setVisibility(View.GONE);
                                            btn.setEnabled(true);
                                            btnModifierPassword.setEnabled(true);


                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        tostMessage("Error de saisie");
                                        spinner.setVisibility(View.GONE);
                                        btn.setEnabled(true);
                                        btnModifierPassword.setEnabled(true);

                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                System.out.println("-----------------------------------------------------------------------");
                                System.out.println("Map ");
                                Map<String, String> params = new HashMap<String, String>();
                                System.out.println("les put");
                                params.put("email", user.getEmail());
                                params.put("role", "Manager");
                                params.put("nom", user.getNom());
                                //params.put("password", user.getPassword());
                                params.put("tel", user.getTel() + "");
                                System.out.println("fin put ");

                                return params;
                            }
                        };
                        postRequest.setRetryPolicy(new RetryPolicy() {
                            @Override
                            public int getCurrentTimeout() {
                                return 50000;
                            }

                            @Override
                            public int getCurrentRetryCount() {
                                return 50000;
                            }

                            @Override
                            public void retry(VolleyError error) throws VolleyError {

                            }
                        });
                        queue.add(postRequest);

                    } else {
                        tostMessage("Error de refaire le mot de passe");
                        spinner.setVisibility(View.GONE);
                        btn.setEnabled(true);
                        btnModifierPassword.setEnabled(true);

                    }
                }
                else {
                    tostMessage("Saisir toutes les champs ils sont obligatoire !");
                    spinner.setVisibility(View.GONE);
                    btn.setEnabled(true);
                    btnModifierPassword.setEnabled(true);

                }
            }
        });

    }

    public boolean verifCHampEmty(){
        if (nom.getText().toString().equals("")||tel.getText().toString().equals("")
                ||email.getText().toString().equals("")){
            return false;
        }
        return true;
    }
    public void SucessInscription(){
        finish();


    }


    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
