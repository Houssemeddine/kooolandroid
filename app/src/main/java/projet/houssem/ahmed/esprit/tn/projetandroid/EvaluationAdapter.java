package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;

import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Avis;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;

public class EvaluationAdapter extends ArrayAdapter<Avis> {

    Context context;
    List<Avis> avis;
    int resources;
    RequestQueue queue ;

    public EvaluationAdapter(@NonNull Context context, int resource, @NonNull List<Avis> objects) {
        super(context, resource, objects);
        this.context=context;
        this.avis = objects;
        this.resources=resource;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resources, null);
        TextView nomUser=convertView.findViewById(R.id.nomUser);
        TextView avistext=convertView.findViewById(R.id.avisText);
        TextView note=convertView.findViewById(R.id.note);

        nomUser.setText(avis.get(position).getIduser().getNom()+"");
        avistext.setText(avis.get(position).getAvis());
        note.setText(" "+avis.get(position).getNote()+"");



return convertView;
    }


}
