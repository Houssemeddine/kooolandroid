package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class PopulaireProduct {

    private Produit produit;
    private int nbr;

    public PopulaireProduct(Produit produit, int nbr) {
        this.produit = produit;
        this.nbr = nbr;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public int getNbr() {
        return nbr;
    }

    public void setNbr(int nbr) {
        this.nbr = nbr;
    }
}
