package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class Subscribe {

    private int id;
    private Pastry idpastry;
    private User iduser;
    private Produit idproduit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pastry getIdpastry() {
        return idpastry;
    }

    public void setIdpastry(Pastry idpastry) {
        this.idpastry = idpastry;
    }

    public User getIduser() {
        return iduser;
    }

    public void setIduser(User iduser) {
        this.iduser = iduser;
    }

    public Produit getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Produit idproduit) {
        this.idproduit = idproduit;
    }

    public Subscribe(int id, Pastry idpastry, User iduser, Produit idproduit) {
        this.id = id;
        this.idpastry = idpastry;
        this.iduser = iduser;
        this.idproduit = idproduit;
    }

    @Override
    public String toString() {
        return "Subscribe{" +
                "id=" + id +
                ", idpastry=" + idpastry +
                ", iduser=" + iduser +
                ", idproduit=" + idproduit +
                '}';
    }
}
