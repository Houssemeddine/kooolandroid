package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class User {
public  static User userConnect;
    private int id;
    private String nom;
    private String password;
    private int tel;
    private String Email;
    private Pastry pastry;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Pastry getPastry() {
        return pastry;
    }

    public void setPastry(Pastry pastry) {
        this.pastry = pastry;
    }

    public User() {
    }

    public User(int id, String nom, String password, int tel, String email) {
        this.id = id;
        this.nom = nom;
        this.password = password;
        this.tel = tel;
        Email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", password='" + password + '\'' +
                ", tel=" + tel +
                ", Email='" + Email + '\'' +
                ", pastry=" + pastry +
                '}';
    }

    public User(int id, String nom, String password, int tel, String email, Pastry pastry) {
        this.id = id;
        this.nom = nom;
        this.password = password;
        this.tel = tel;
        Email = email;
        this.pastry = pastry;
    }
}
