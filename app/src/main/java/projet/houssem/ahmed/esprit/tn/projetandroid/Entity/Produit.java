package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class Produit {

    private int id;
    private int prix;
    private String nom;
    private String type;
    private Description iddesc;
    private Pastry idpastry;
    private  String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Produit(int id, int prix, String nom, Description description, Pastry pastry) {
        this.id = id;
        this.prix = prix;
        this.nom = nom;
        this.iddesc = description;
        this.idpastry = pastry;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }



    public Pastry getIdpastry() {
        return idpastry;
    }

    public void setIdpastry(Pastry pastry) {
        this.idpastry = pastry;
    }

    public Description getIddesc() {
        return iddesc;
    }

    public void setIddesc(Description iddesc) {
        this.iddesc = iddesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", prix=" + prix +
                ", nom='" + nom + '\'' +
                ", type='" + type + '\'' +
                ", iddesc=" + iddesc +
                ", idpastry=" + idpastry +
                '}';
    }
}
