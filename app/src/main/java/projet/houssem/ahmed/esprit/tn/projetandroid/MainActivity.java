package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    //ActionBar toolbar;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.profile:
                    getFragmentManager().beginTransaction().replace(R.id.frame,new ProfileFragment()).commit();
                    return true;
                case R.id.search:
                    getFragmentManager().beginTransaction().replace(R.id.frame,new AccueilFragment()).commit();                    return true;
                case R.id.share:
                    getFragmentManager().beginTransaction().replace(R.id.frame,new AcueilProductFragment()).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // toolbar = getSupportActionBar();
        getFragmentManager().beginTransaction().replace(R.id.frame,new AccueilFragment()).commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


    }

}
