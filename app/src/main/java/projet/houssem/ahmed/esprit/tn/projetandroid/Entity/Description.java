package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class Description {
    private  int id;
    private String bigdesc;
    private String smalldesc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBigdesc() {
        return bigdesc;
    }

    public void setBigdesc(String bigdesc) {
        this.bigdesc = bigdesc;
    }

    public String getSmalldesc() {
        return smalldesc;
    }

    public void setSmalldesc(String smalldesc) {
        this.smalldesc = smalldesc;
    }

    @Override
    public String toString() {
        return "description{" +
                "id=" + id +
                ", bigdesc='" + bigdesc + '\'' +
                ", smalldesc='" + smalldesc + '\'' +
                '}';
    }

    public Description(int id, String bigdesc, String smalldesc) {
        this.id = id;
        this.bigdesc = bigdesc;
        this.smalldesc = smalldesc;
    }

    public Description() {
    }
}
