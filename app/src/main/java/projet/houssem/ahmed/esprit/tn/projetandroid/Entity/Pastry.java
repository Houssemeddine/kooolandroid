package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class Pastry {
    private int id;
    private int tel;
    private Description iddesc;
    private String adresse;
    private String nom;
    private Double 	jpsLongitude;
    private Double 	jpsLatitude;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public Description getDesc() {
        return iddesc;
    }

    public void setDesc(Description desc) {
        this.iddesc = desc;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        nom = nom;
    }


    public Pastry(int id, int tel, Description iddesc, String adresse, String nom, Double jpslongitude, Double jpslatitude) {
        this.id = id;
        this.tel = tel;
        this.iddesc = iddesc;
        this.adresse = adresse;
        this.nom = nom;
        this.jpsLongitude = jpslongitude;
        this.jpsLatitude = jpslatitude;
    }

    public Double getJpsLongitude() {
        return jpsLongitude;
    }

    public void setJpsLongitude(Double jpsLongitude) {
        this.jpsLongitude = jpsLongitude;
    }

    public Double getJpslongitude() {
        return jpsLongitude;
    }

    public void setJpslongitude(Double jpslongitude) {
        this.jpsLatitude = jpslongitude;
    }

    @Override
    public String toString() {
        return "Pastry{" +
                "id=" + id +
                ", tel=" + tel +
                ", iddesc=" + iddesc +
                ", adresse='" + adresse + '\'' +
                ", nom='" + nom + '\'' +
                ", jpslongitude=" + jpsLongitude +
                ", jpslatitude=" + jpsLatitude +
                '}';
    }

    public Double getJpsLatitude() {
        return jpsLatitude;
    }

    public void setJpsLatitude(Double jpslatitude) {
        this.jpsLatitude = jpslatitude;
    }

    public Pastry() {
    }

    public Description getIddesc() {
        return iddesc;
    }

    public void setIddesc(Description iddesc) {
        this.iddesc = iddesc;
    }
}
