package projet.houssem.ahmed.esprit.tn.projetandroid.Utilite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Date;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Koul.db";
    private static final int DATABASE_VERSION = 1;
    public DatabaseManager(Context context){
        super( context, DATABASE_NAME,null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String strSql = "create table derniervisiter("
                      + "     id integer primary key autoincrement,"
                      + "     idpat integer not null,"
                      + "     quand integer not null"
                      + "     )";
        db.execSQL(strSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //ne rien faire
    }

    public void insererLigne(int id){
        String strSql = "Insert into derniervisiter (idpat,quand) values ( "
                      + id + ","
                      + new Date().getTime()
                      + ")";
        boolean exist = test(id);
        if(!exist)
        this.getWritableDatabase().execSQL(strSql);
    }

    public ArrayList<Integer> getLast6(){
        ArrayList<Integer> res = new ArrayList<>();
        String strSql = "select idpat from derniervisiter";
        Cursor cursor = this.getReadableDatabase().rawQuery(strSql,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            int temId = cursor.getInt(0);
            res.add(temId);
            cursor.moveToNext();
        }
        cursor.close();
        return res;
    }
    private boolean test(int demp){
        ArrayList<Integer> tempList = getLast6();
        for(int i : tempList){
            if(i == demp )
                return true;
        }
        return false;
    }
}
