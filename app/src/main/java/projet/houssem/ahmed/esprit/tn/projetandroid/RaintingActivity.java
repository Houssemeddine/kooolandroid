package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class RaintingActivity extends AppCompatActivity {
int id;

Button btnavis;
    private RatingBar ratingBar;
    private TextView txtRatingValue;
    private TextView text;
    Intent intent;
    String name;
    EditText avisText;
    RequestQueue queue ;
    private ProgressBar spinner;

    String urlAvis= Ressource.url+"/api/tnotes";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rainting);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);
         intent = getIntent();
         String Evaluation = intent.getStringExtra("Evaluation");

        btnavis= (Button)findViewById(R.id.btnavis);
        avisText=(EditText)findViewById(R.id.avisText);
        queue = Volley.newRequestQueue(this);
        text=(TextView)findViewById(R.id.text);

        addListenerOnRatingBar();


        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
         if (Evaluation.equals("Pastry")){
             String value = intent.getStringExtra("idpastry");
             name = intent.getStringExtra("nom");
             id= Integer.parseInt(value);
             btnavis.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     spinner.setVisibility(View.VISIBLE);

                     ajoutavis(id);
                 }
             });
         }else {
             String value = intent.getStringExtra("idproduit");
             name = intent.getStringExtra("nom");
             id= Integer.parseInt(value);
             text.setText("Que pense-tu de ce produit?");
             btnavis.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     spinner.setVisibility(View.VISIBLE);

                     ajoutavisProduit(id);
                 }
             });

         }



    setTitle("Pâtisserie"+intent.getStringExtra("nom"));

    }
    public void getpastry(int id,String name){
       finish();
    }
    public void addListenerOnRatingBar() {

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        txtRatingValue = (TextView) findViewById(R.id.note);

        //if rating value is changed,
        //display the current rating value in the result (textview) automatically
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                txtRatingValue.setText(String.valueOf(rating));

            }
        });
    }

    public void ajoutavisProduit(final int idpastry){
        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlAvis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);

                            getpastry(id,name);
                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();
                            spinner.setVisibility(View.GONE);


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());
                        spinner.setVisibility(View.GONE);

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", idpastry  + "");
                params.put("note",  ratingBar.getRating() + "");
                params.put("iduser", User.userConnect.getId() + "");
                params.put("avis", avisText.getText().toString() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);



    }
    public void ajoutavis(final int idpastry){
        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlAvis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);

                            getpastry(id,name);
                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();
                            spinner.setVisibility(View.GONE);


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());
                        spinner.setVisibility(View.GONE);

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", idpastry  + "");
                params.put("note",  ratingBar.getRating() + "");
                params.put("iduser", User.userConnect.getId() + "");
                params.put("avis", avisText.getText().toString() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);



    }
}
