package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class CatalogueProductAcueilActivity extends AppCompatActivity {
    ListView lstproduit;
    RequestQueue queue ;
    String urlProduit = Ressource.url+"/api/tproduits";
int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue_product_acueil);
        lstproduit = (ListView) findViewById(R.id.listcatalogue);
        Intent intent = getIntent();
        String value = intent.getStringExtra("id");
        id= Integer.parseInt(value);
        queue = Volley.newRequestQueue(this);

        getProduit(id);
    }

    public void getProduit(final int id){
        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlProduit,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Produit>>() {
                        }.getType();
                        List<Produit> produits = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt");
                        System.out.println("LIST : " + produits);
                        List<Produit> produitsfromPastry= new ArrayList<>();
                        for ( Produit produit:produits) {
                            if (produit.getIdpastry().getId()== id){
                                produitsfromPastry.add(produit);
                            }

                        }
                        final ListProductAdapter adapter = new ListProductAdapter(  getApplicationContext(), R.layout.item_catalogue_acueil, produitsfromPastry);

                        lstproduit.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }


}
