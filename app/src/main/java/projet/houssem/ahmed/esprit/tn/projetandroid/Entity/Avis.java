package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

import java.util.Date;

public class Avis {

    private int id;
    private String avis;
    private int note;
    private Date date;
    private Pastry idpastry;
    private User iduser;
    private Produit idproduit;

    public Produit getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Produit idproduit) {
        this.idproduit = idproduit;
    }

    @Override
    public String toString() {
        return "Avis{" +
                "id=" + id +
                ", avis='" + avis + '\'' +
                ", note=" + note +
                ", date=" + date +
                ", idpastry=" + idpastry +
                ", iduser=" + iduser +
                ", idproduit=" + idproduit +
                '}';
    }

    public Avis(int id, String avis, int note, Date date, Pastry idpastry, User iduser) {
        this.id = id;
        this.avis = avis;
        this.note = note;
        this.date = date;
        this.idpastry = idpastry;
        this.iduser = iduser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Pastry getIdpastry() {
        return idpastry;
    }

    public void setIdpastry(Pastry idpastry) {
        this.idpastry = idpastry;
    }

    public User getIduser() {
        return iduser;
    }

    public void setIduser(User iduser) {
        this.iduser = iduser;
    }
}
