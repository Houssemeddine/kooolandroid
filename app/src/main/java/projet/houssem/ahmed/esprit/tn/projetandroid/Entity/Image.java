package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class Image {
    private  int id;
    private  String UrlImage;
    private String legende;
    private Pastry idpastry;
    private Produit idproduit;

    public Image(int id, String urlImage) {
        this.id = id;
        UrlImage = urlImage;
        this.legende = legende;
    }

    public Image(int id, String urlImage, String legende, Produit produit) {
        this.id = id;
        UrlImage = urlImage;
        this.legende = legende;
        this.idproduit = produit;
    }

    public Produit getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Produit produit) {
        this.idproduit = produit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlImage() {
        return UrlImage;
    }

    public void setUrlImage(String urlImage) {
        UrlImage = urlImage;
    }

    public String getLegende() {
        return legende;
    }

    public void setLegende(String legende) {
        this.legende = legende;
    }

    public Pastry getIdpastry() {
        return idpastry;
    }

    public void setIdpastry(Pastry pastry) {
        this.idpastry = pastry;
    }

    public Image(int id, String urlImage, String legende, Pastry pastry) {
        this.id = id;
        UrlImage = urlImage;
        this.legende = legende;
        this.idpastry = pastry;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", UrlImage='" + UrlImage + '\'' +
                ", legende='" + legende + '\'' +
                ", idpastry=" + idpastry +
                ", idproduit=" + idproduit +
                '}';
    }
}
