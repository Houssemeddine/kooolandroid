package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class CatalogueActivity extends AppCompatActivity  {
    ListView lstproduit;
    RequestQueue queue ;
    String urlProduit = Ressource.url+"/api/tproduits";
    SwipeRefreshLayout refresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogue);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CatalogueActivity.this,AddCatalogueActivity.class);

                startActivity(intent);

            }
        });
        setTitle("Vos produits");


        queue = Volley.newRequestQueue(this);

        lstproduit = (ListView) findViewById(R.id.listcatalogue);
        getProduit();
         refresh=(SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProduit();
                refresh.setRefreshing(false);
            }
        });



    }

    public void getProduit(){
       // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlProduit,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                       try {
                           Log.d("Response", response.toString());JSONObject jsonobject = null;
                           Type listType = new TypeToken<ArrayList<Produit>>() {
                           }.getType();
                           List<Produit> produits = new GsonBuilder().create().fromJson(response.toString(), listType);
                           System.out.println("LIIIIIIIIIIIIIIIISt");
                           System.out.println("LIST : " + produits);
                           List<Produit> produitsfromPastry= new ArrayList<>();
                           for ( Produit produit:produits) {
                               if (produit.getIdpastry().getId()==User.userConnect.getPastry().getId()){
                                   produitsfromPastry.add(produit);
                               }

                           }
                           final GererCatalogueAdapter adapter = new GererCatalogueAdapter(  getApplicationContext(), R.layout.item_catalogue, produitsfromPastry);

                           adapter.notifyDataSetChanged();
                           lstproduit.setAdapter(adapter);
                       }catch (Exception e){
                           getProduit();
                       }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }

}
