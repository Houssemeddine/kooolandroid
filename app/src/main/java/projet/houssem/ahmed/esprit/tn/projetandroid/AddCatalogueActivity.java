package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Description;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class AddCatalogueActivity extends AppCompatActivity {

    EditText petitDesc;
    EditText grandDesc;
    EditText NomProduit;
    EditText Prix;

    Button btnImage;
    Button btnAjouPastry;
    ImageView imageProduit;
    RequestQueue queue ;

    private Spinner typeProduit;
    boolean ajoutImage = false;


    private ProgressBar spinner;
    ScrollView scrollView ;


    private final  int IMG_Request = 1;
    Bitmap bitmap;
    Description description;
    Image image;
    Produit produit;

    String urlDes = Ressource.url+"/api/tdescriptions";
    String urlImageupload =Ressource.url+"/ImagePastry";

    String urlImage =Ressource.url+"/api/tphotos";
    String urlProduit =Ressource.url+"/api/tproduits";
    Double legende;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_catalogue);

        queue = Volley.newRequestQueue(this);
        petitDesc = (EditText)findViewById(R.id.petitDesc);
        grandDesc = (EditText)findViewById(R.id.grandDesc);
        NomProduit = (EditText)findViewById(R.id.nomProduit);
        Prix = (EditText)findViewById(R.id.prix);
legende=Math.random() * ( 999999 - 111111 );
        typeProduit=(Spinner)findViewById(R.id.typeproduit);
scrollView=(ScrollView)findViewById(R.id.contenue);

imageProduit=(ImageView) findViewById(R.id.imageViewProduit);
        btnAjouPastry=(Button)findViewById(R.id.btnAjoutProduit);
        btnAjouPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (verifCHampEmty()){
                    if (ajoutImage){
                spinner.setVisibility(View.VISIBLE);
                scrollView.setEnabled(false);
                btnAjouPastry.setEnabled(false);
                petitDesc.setEnabled(false);
                grandDesc.setEnabled(false);
                NomProduit.setEnabled(false);
                Prix.setEnabled(false);
                ajoutDescription();}
                else {
                        tostMessage("l'ajout d'une image c'est obligatoire !");
                        spinner.setVisibility(View.GONE);
                        scrollView.setEnabled(true);
                        btnAjouPastry.setEnabled(true);
                        petitDesc.setEnabled(true);
                        grandDesc.setEnabled(true);
                        NomProduit.setEnabled(true);
                        Prix.setEnabled(true);
                    }
                }else {
                    tostMessage("Saisir toutes les champs ils sont obligatoire !");
                    spinner.setVisibility(View.GONE);
                    scrollView.setEnabled(true);
                    btnAjouPastry.setEnabled(true);
                    petitDesc.setEnabled(true);
                    grandDesc.setEnabled(true);
                    NomProduit.setEnabled(true);
                    Prix.setEnabled(true);
                }
            }

        });
        btnImage =(Button)findViewById(R.id.buttonUploadImageProduit);
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selctImageFromGalerie();

                ajoutImage=true;
                btnImage.setText("modifier l'image");
            }
        });


        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);

    }



    public void ajoutDescription(){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlDes,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {

                            jsonobject = new JSONObject(response);
                            description = new Description(jsonobject.getInt("id"),jsonobject.getString("bigdesc"),jsonobject.getString("smalldesc"));
                            ajoutProduit(legende);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response ajout description", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("bigdesc", grandDesc.getText().toString());
                params.put("smalldesc", petitDesc.getText().toString());
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }
    private  void selctImageFromGalerie(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMG_Request);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==IMG_Request&&resultCode==RESULT_OK&&data !=null){
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                imageProduit.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void UploadImage(){

        StringRequest postRequest = new StringRequest(Request.Method.POST, urlImageupload,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);

                            System.out.println("les reponse in upload image ");
                            if (jsonobject.getString("password").equals("sucess")){

                                //User.userConnect.setPastry(pastry);
                                SuccesajoutProduit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la respose in upload photos ");
                            finish();

                        }



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;
                        SuccesajoutProduit();
                        //Toast.makeText(context, "Error  d'upload image", duration).show();
                        finish();

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("image", imageToString(bitmap));
                params.put("urlphotos", "C:/wamp64/www/ServiceMobile/mobileService/web/upload/");
                params.put("legende",  image.getLegende());
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }
    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte [] imgbyte= byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(imgbyte,Base64.DEFAULT);

    }
    public void ajoutImage(){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlImage,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            image  = new Image(jsonobject.getInt("id")
                                    ,jsonobject.getString("urlphotos"),jsonobject.getString("legende"),produit);

                            UploadImage();
                            //ajoutProduit(jsonobject.getString("legende"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        //Toast.makeText(context, "Error  d'response ajout image", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("urlphotos", "C:/wamp64/www/ServiceMobile/mobileService/web/upload/");
                params.put("legende",  legende+"");
                params.put("idproduit", produit.getId()+"");
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }


    public void ajoutProduit(final Double image){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlProduit,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
produit = new Produit(jsonobject.getInt("id"), jsonobject.getInt("prix"),jsonobject.getString("nom"),description,User.userConnect.getPastry());
produit.setImage(image+"");
                            ajoutImage();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'ajout produit", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("prix", Prix.getText().toString()+"");
                params.put("nom", NomProduit.getText().toString());
                params.put("image", image+"");
                params.put("type", typeProduit.getSelectedItem().toString());
                params.put("iddesc",  description.getId()+"");
                params.put("idpastry",  User.userConnect.getPastry().getId()+"");

                System.out.println("fin put ");

                return params;
            }
        };


        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }
    public void SuccesajoutProduit(){
       finish();

    }
    public boolean verifCHampEmty(){
        if (NomProduit.getText().toString().equals("")||Prix.getText().toString().equals("")
                ||petitDesc.getText().toString().equals("")
                ||grandDesc.getText().toString().equals("")
                ){
            return false;
        }
        return true;
    }
    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }}
