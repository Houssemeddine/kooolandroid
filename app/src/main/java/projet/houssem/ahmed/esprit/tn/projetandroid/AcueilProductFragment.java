package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.PopulairePastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.PopulaireProduct;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AcueilProductFragment extends Fragment {

    RequestQueue queue ;
    ListView listView;
    Button btnplusPastry;
    Button btncake;
    Button btnjuice;
    Button btnplus;
    EditText searchtext;

    String urlPastry = Ressource.url+"/api/tproduits";
    String urlPhotos =Ressource.url+"/api/tphotos";
    String urlgetPopulairePastry = Ressource.url+"/getPopulaireProduct";
    String urlNewPastry = Ressource.url+"/getNewProduct";
    ListProductAdapter adapter;
    List<Produit> pastrys;
    SwipeRefreshLayout refresh;

    //recycleview pour l'affichage horizentalenmet

    LinearLayoutManager layoutManager;
    RecyclerView myList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_acueil_produit, container, false);

        listView = (ListView)root.findViewById(R.id.listPastry);
        btnplusPastry=(Button)root.findViewById(R.id.btnplus) ;
        btncake=(Button)root.findViewById(R.id.BtnCake) ;
        btnjuice=(Button)root.findViewById(R.id.btnjuice) ;
        btnplus=(Button)root.findViewById(R.id.btnmore) ;

        btnplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("choisir un type de produit");

// add a radio button list
                final String[] animals = {"petit four", "baguettes", "pizza", "sandwich", "gateau au chocolat","gateau aux fruits","croissants","pain chocolat","tarte"};
                 final int[] checkedItem = {1}; // cow
                builder.setSingleChoiceItems(animals, checkedItem[0], new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // user checked an item
                        System.out.println("cest which de button check item : "+which);
                        int a=which;
                        checkedItem[0] =a;



                    }
                });

// add OK and Cancel buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(getActivity(),SelectProductActivity.class);
                        System.out.println("c'est chek item "+ checkedItem[0]);
                        System.out.println("c'est animals "+animals[checkedItem[0]]);
                        System.out.println("cest which de button ok : "+which);
                        i.putExtra("type",animals[checkedItem[0]]);

                        startActivityForResult(i,1000);                    }
                });
                builder.setNegativeButton("Annuler", null);

// create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        searchtext=(EditText)root.findViewById(R.id.search);
        searchtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),SelectProductActivity.class);
                i.putExtra("type","tous");

                startActivityForResult(i,1000);

            }
        });
        //horizental affiche
        btncake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),SelectProductActivity.class);
                i.putExtra("type","gateaux");

                startActivityForResult(i,1000);
            }
        });btnjuice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),SelectProductActivity.class);
                i.putExtra("type","jus");

                startActivityForResult(i,1000);
            }
        });
        btnplusPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),SelectProductActivity.class);
                i.putExtra("type","tous");

                startActivityForResult(i,1000);
            }
        });



        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        myList = (RecyclerView) root.findViewById(R.id.framehoriz);
        myList.setLayoutManager(layoutManager);

        getNewProduit();
        getPopulaireProduit();
        refresh=(SwipeRefreshLayout)root.findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewProduit();
                getPopulaireProduit();
                refresh.setRefreshing(false);
            }
        });
        return root;
    }


    public void getNewProduit(){
        queue = Volley.newRequestQueue(getActivity());

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlNewPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                       try {
                           Log.d("Response", response.toString());
                           JSONObject jsonobject = null;
                           Type listType = new TypeToken<ArrayList<Produit>>() {
                           }.getType();
                           pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                           System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);


                           recycleAccueilProductAdapter horizentallistAdapter=new recycleAccueilProductAdapter(getActivity(),pastrys);
                           myList.setAdapter(horizentallistAdapter);
                           adapter.notifyDataSetChanged();
                       }
                       catch (Exception e){
                           System.out.println("error dans la methode response onErrorResponse");
                           Context context = getApplicationContext();
                           int duration = Toast.LENGTH_SHORT;

                          // Toast.makeText(context, "Error  de connexion", duration).show();
                       }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                      //  Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }
    public void getPopulaireProduit(){
        queue = Volley.newRequestQueue(getActivity());

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlgetPopulairePastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                       try {
                           Log.d("Response", response.toString());
                           JSONObject jsonobject = null;
                           Type listType = new TypeToken<ArrayList<PopulaireProduct>>() {
                           }.getType();
                           List<PopulaireProduct> populairePastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                           System.out.println("LIIIIIIIIIIIIIIIISt"+populairePastrys);
                           List<Produit> pastrys= new ArrayList<>();
                           for (PopulaireProduct populairePastry1:populairePastrys) {
                               pastrys.add(populairePastry1.getProduit());
                           }

                           adapter = new ListProductAdapter(  getApplicationContext(), R.layout.item_catalogue_acueil, pastrys);

                           listView.setAdapter(adapter);


                       }
                       catch (Exception e){
                           System.out.println("error dans la methode response onErrorResponse");
                           Context context = getApplicationContext();
                           int duration = Toast.LENGTH_SHORT;

                          // Toast.makeText(context, "Error  de connexion", duration).show();
                       }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                      //  Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }

}
