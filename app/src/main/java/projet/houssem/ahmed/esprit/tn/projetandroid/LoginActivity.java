package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Description;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class LoginActivity extends AppCompatActivity {


    TextView textErreur;
    EditText email;
    EditText password;
    Button btnLogin;
    Button btnRegister;
    RequestQueue queue ;
    RegisterActivity registerActivity;
    SharedPreferences pref ;// 0 - for private mode
    private ProgressBar spinner;

    String url = Ressource.url+"/login";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref = getApplicationContext().getSharedPreferences("mypref", Context.MODE_PRIVATE); // 0 - for private mode
        if (pref.getBoolean("connexion", false)) {

            SucessInscription();
        }
        btnRegister = (Button) findViewById(R.id.btnregister);
        btnLogin = (Button) findViewById(R.id.btnConneter);
         email = (EditText) findViewById(R.id.email);
         password = (EditText) findViewById(R.id.password);
         btnLogin = (Button) findViewById(R.id.btnConneter);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);
        queue = Volley.newRequestQueue(this);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setContentView(R.layout.activity_accueil);
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading(true);

                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response.toString());

                                JSONObject jsonobject = null;
                                try {
                                    System.out.println("entrer dans la recuperaation des donnes");
                                    jsonobject = new JSONObject(response);
                                    System.out.println(jsonobject);

                                    if (jsonobject.isNull("idpastry")){

                                        System.out.println("user qui n'a pas ajouter encore une patisseurie");
                                        User.userConnect= new User(jsonobject.getInt("id"),jsonobject.getString("nom")
                                                ,jsonobject.getString("password"),jsonobject.getInt("tel"),jsonobject.getString("email"));

                                    }else {
                                        Description description = new Description(
                                                jsonobject.getJSONObject("idpastry").getJSONObject("iddesc").getInt("id"),
                                                jsonobject.getJSONObject("idpastry").getJSONObject("iddesc").getString("bigdesc"),
                                                jsonobject.getJSONObject("idpastry").getJSONObject("iddesc").getString("smalldesc"));


                                        Pastry pastry = new Pastry(
                                                jsonobject.getJSONObject("idpastry").getInt("id"),
                                                jsonobject.getJSONObject("idpastry").getInt("tel")
                                                , description,
                                                jsonobject.getJSONObject("idpastry").getString("adresse")
                                                , jsonobject.getJSONObject("idpastry").getString("nom"),
                                                jsonobject.getJSONObject("idpastry").getDouble("jpsLongitude")
                                                ,jsonobject.getJSONObject("idpastry").getDouble("jpsLatitude"));


                                        User.userConnect = new User(jsonobject.getInt("id"), jsonobject.getString("nom")
                                                , jsonobject.getString("password"), jsonobject.getInt("tel"), jsonobject.getString("email"), pastry);
                                    }
                                    System.out.println(User.userConnect);
                                    authPreference(true);
                                    SucessInscription();
                                } catch (JSONException e) {
                                    System.out.println("error dans la response jsonObject");
                                    e.printStackTrace();
                                    Context context = getApplicationContext();
                                    int duration = Toast.LENGTH_SHORT;
                                    loading(false);

                                    Toast.makeText(context, "Error  d'identification", duration).show();

                                }

                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                System.out.println("error dans la methode response onErrorResponse");
                                Log.d("Error.Response", error.toString());
                                Context context = getApplicationContext();
                                int duration = Toast.LENGTH_SHORT;
                                loading(false);

                                Toast.makeText(context, "Error  d'identification", duration).show();
                            }
                        }
                ){
                    @Override
                    protected Map<String, String> getParams()
                    {
                        System.out.println("-----------------------------------------------------------------------");
                        System.out.println("Map ");
                        Map<String, String> params = new HashMap<String, String>();

                        System.out.println("les put");
                        params.put("email", email.getText().toString());
                        params.put("password", password.getText().toString());
                        System.out.println("fin put ");

                        return params;
                    }
                };
                postRequest.setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 50000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 50000;
                    }

                    @Override
                    public void retry(VolleyError error) throws VolleyError {

                    }
                });
                queue.add(postRequest);




            }
        });



    }
    public void SucessInscription(){
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);


    }
    public void authPreference(boolean a){

        pref = getApplicationContext().getSharedPreferences("mypref", Context.MODE_PRIVATE); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("connexion",a);
        editor.putInt("idUser",User.userConnect.getId());
        editor.commit();


    }

    public void  loading(Boolean t){
        if(t){
            spinner.setVisibility(View.VISIBLE);

        }else {
            spinner.setVisibility(View.GONE);
        }
        email.setEnabled(!t);
        password.setEnabled(!t);
        btnLogin.setEnabled(!t);
        btnRegister.setEnabled(!t);
    }
}
