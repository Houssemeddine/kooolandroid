package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class PopulairePastry {

    private Pastry pastry;
    private int nbr ;

    public PopulairePastry(Pastry pastry, int nbr) {
        this.pastry = pastry;
        this.nbr = nbr;
    }

    public Pastry getPastry() {
        return pastry;
    }

    public void setPastry(Pastry pastry) {
        this.pastry = pastry;
    }

    public int getNbr() {
        return nbr;
    }

    public void setNbr(int nbr) {
        this.nbr = nbr;
    }
}
