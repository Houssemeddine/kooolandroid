package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SelectProductActivity extends AppCompatActivity {
    RequestQueue queue ;
    ListView listView;
    String urlPastry = Ressource.url+"/api/tproduits";
    String urlPhotos =Ressource.url+"/api/tphotos";
    ListProductAdapter adapter;
    List<Produit> pastrys;
    Intent intent;
    TextView search;
    MaterialSearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_product);
        intent = getIntent();
        String type = intent.getStringExtra("type");
        listView = (ListView)findViewById(R.id.listPastry);
setTitle("");
        searchView = (MaterialSearchView)findViewById(R.id.search_view);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

                getProduit();

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null && !newText.isEmpty()){
                    List<String> lstFound = new ArrayList<String>();
                    getProduitsearch(newText);
                }
                else{
                    //if search text is null
                    //return default
                    getProduit();

                }
                return true;
            }

        });


        if (type.equals("tous")){
            getProduit();
        }else {
            getProduitWithType(type);

        }

    }


    public void getProduit(){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            Log.d("Response", response.toString());
                            JSONObject jsonobject = null;
                            Type listType = new TypeToken<ArrayList<Produit>>() {
                            }.getType();
                            pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);


                            adapter = new ListProductAdapter(  getApplicationContext(), R.layout.item_catalogue_acueil, pastrys);

                            listView.setAdapter(adapter);

//                            recycleAccueilProductAdapter horizentallistAdapter=new recycleAccueilProductAdapter(this,pastrys);
//                            myList.setAdapter(horizentallistAdapter);
                            adapter.notifyDataSetChanged();
                        }
                        catch (Exception e){
                            System.out.println("error dans la methode response onErrorResponse");
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;

                            Toast.makeText(context, "Error  de connexion", duration).show();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }
    public void getProduitWithType(final String type){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            Log.d("Response", response.toString());
                            JSONObject jsonobject = null;
                            Type listType = new TypeToken<ArrayList<Produit>>() {
                            }.getType();
                            pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                            List<Produit> produits =  new ArrayList<>();
                            for (Produit p:pastrys){
                                if (p.getType().equals(type))
                                    produits.add(p);

                            }

                            adapter = new ListProductAdapter(  getApplicationContext(), R.layout.item_catalogue_acueil, produits);

                            listView.setAdapter(adapter);

//                            recycleAccueilProductAdapter horizentallistAdapter=new recycleAccueilProductAdapter(this,pastrys);
//                            myList.setAdapter(horizentallistAdapter);
                            adapter.notifyDataSetChanged();
                        }
                        catch (Exception e){
                            System.out.println("error dans la methode response onErrorResponse");
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;

                            //Toast.makeText(context, "Error  de connexion", duration).show();
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                       // Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }
    public void getProduitsearch(final String newText){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Produit>>() {
                        }.getType();
                        try {
                            List<Produit> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                            List<Produit> lstFound = new ArrayList<Produit>();
                            for(Produit item:pastrys){
                                if(item.getNom().contains(newText))
                                    lstFound.add(item);
                            }



                            final ListProductAdapter adapter = new ListProductAdapter(  getApplicationContext(), R.layout.item_catalogue_acueil, lstFound);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                        }catch (Exception e){

                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                      //  Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }
}
