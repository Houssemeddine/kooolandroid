package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.ProximityPastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class ProximituPastryAccueilAdapter extends ArrayAdapter<ProximityPastry> {
    Context context;
    List<ProximityPastry> pastry;
    int resources;
    RequestQueue queue ;
    String urlPhotos = Ressource.url+"/api/tphotos";
    String urlAbonnerpastry =Ressource.url+"/abonnerPastry";
    String urldesAbonnerpastry =Ressource.url+"/desabonnerPastry";
    String urlverifAbonner =Ressource.url+"/verifierAbonnerPastry";

    String urlnbrAbonner =Ressource.url+"/SumAabonnerPastry";


    public ProximituPastryAccueilAdapter(@NonNull Context context, int resource, @NonNull List<ProximityPastry> objects) {
        super(context, resource, objects);
        this.context=context;
        this.pastry = objects;
        this.resources=resource;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
        LayoutInflater inflater= LayoutInflater.from(context);
        convertView = inflater.inflate(resources, null);

        final ProgressBar spinner = (ProgressBar)convertView.findViewById(R.id.progressBar1);

        TextView desc = (TextView) convertView.findViewById(R.id.petitDesc);
        TextView nomPastry = (TextView) convertView.findViewById(R.id.nomPastry);
        TextView adresse = (TextView) convertView.findViewById(R.id.adresse);
        TextView catalogue = (TextView) convertView.findViewById(R.id.catalogue);


        final TextView numAbonner = (TextView) convertView.findViewById(R.id.numAbonner);
        final TextView like = (TextView) convertView.findViewById(R.id.like);
        final ImageView imagepastry = (ImageView)convertView.findViewById(R.id.photo);
        final ImageView likeImg = (ImageView)convertView.findViewById(R.id.likeImg);
        imagepastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpastry(pastry.get(position).getPastry());

            }
        });
        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProduct(pastry.get(position).getPastry().getId());
            }
        });

        desc.setText(pastry.get(position).getPastry().getDesc().getSmalldesc());
        nomPastry.setText(pastry.get(position).getPastry().getNom());
        adresse.setText(pastry.get(position).getPastry().getAdresse());
        queue = Volley.newRequestQueue(context);


        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("clik");
                if (User.userConnect==null){
                    login();

                }else
                {
                    if (like.getText().equals("se désabonner")){
                        desabonnerPastry(pastry.get(position).getPastry().getId());
                        like.setText("s'abonner");
                        likeImg.setImageResource(R.drawable.like);

                    }else {
                        abonner(pastry.get(position).getPastry().getId());
                        like.setText("se désabonner");
                        likeImg.setImageResource(R.drawable.favorite);

                    }
                }

            }
        });

//calculer le nbr d'abonner

        StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlnbrAbonner,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            numAbonner.setText(jsonobject.getInt("somme")+" abonné(s)");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", pastry.get(position).getPastry().getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAbonner);

        //voir si l'utilisateurs connecter est abonner cette pastry ou non
        if (User.userConnect!=null) {
            StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlverifAbonner,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            Log.d("Response", response.toString());

                            JSONObject jsonobject = null;
                            try {
                                System.out.println("entrer dans la recuperaation des donnes");
                                jsonobject = new JSONObject(response);

                                System.out.println(jsonobject);
                                if (jsonobject.getBoolean("subscribe")) {

                                    System.out.println("true subscribe");
                                    like.setText("se désabonner");
                                    likeImg.setImageResource(R.drawable.favorite);

                                }

                            } catch (JSONException e) {
                                System.out.println("error dans la response jsonObject");
                                e.printStackTrace();


                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                            Log.d("Error.Response", error.toString());

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    System.out.println("-----------------------------------------------------------------------");
                    System.out.println("Map ");
                    Map<String, String> params = new HashMap<String, String>();

                    System.out.println("les put");
                    params.put("pastry", pastry.get(position).getPastry().getId() + "");
                    params.put("user", User.userConnect.getId() + "");
                    System.out.println("fin put ");

                    return params;
                }
            };
            postRequestAbonner.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            queue.add(postRequestAbonner);




        }
//insertion photo de la pastry

        //insertion photo de la pastry
        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+pastry.get(position).getPastry().getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                       imagepastry.setImageBitmap(bitmap);
                        spinner.setVisibility(View.GONE);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imagepastry.setImageResource(R.drawable.logo);
                        spinner.setVisibility(View.GONE);

                    }
                });
        queue.add(request);

        return convertView;
    }
    public void login(){
        Intent intent = new Intent(getContext(),LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    public void getProduct(int id){
        Intent intent = new Intent(getContext(),CatalogueProductAcueilActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",id+"");
        getContext().startActivity(intent);
    }

    public  void abonner(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            if (!jsonobject.getBoolean("abonner")){
                                int duration = Toast.LENGTH_SHORT;


                                Toast.makeText(context, "vous étiez déjà abonner", duration).show();
                            }


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);

    }
    public void desabonnerPastry(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urldesAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);


    }

    public void getpastry(Pastry pastry){

        Intent intent = new Intent(getContext(),PastryFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        getContext().startActivity(intent);

    }

}
