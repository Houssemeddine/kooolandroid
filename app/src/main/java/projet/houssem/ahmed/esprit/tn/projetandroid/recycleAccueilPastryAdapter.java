package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class recycleAccueilPastryAdapter extends RecyclerView.Adapter<recycleAccueilPastryAdapter.ViewHolder> {

    private List<Pastry> pastry;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    int resources;
    private Context context;
    RequestQueue queue ;
    String urlPhotos = Ressource.url+"/api/tphotos";
    String urlAbonnerpastry =Ressource.url+"/abonnerPastry";
    String urldesAbonnerpastry =Ressource.url+"/desabonnerPastry";
    String urlverifAbonner =Ressource.url+"/verifierAbonnerPastry";
    String urlnbrAbonner =Ressource.url+"/SumAabonnerPastry";
    // data is passed into the constructor
    recycleAccueilPastryAdapter(Context context, List<Pastry> animals) {
        this.mInflater = LayoutInflater.from(context);
        this.pastry = animals;
        this.context=context;

    }

    // inflates the row layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_accueil_pastry_horizentalement, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        //int color = mViewColors.get(position);
        //String animal = mAnimals.get(position);
        holder.imagepastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpastry(pastry.get(position));

            }
        });
        holder.catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProduct(pastry.get(position).getId());
            }
        });
 holder.cataloguetext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProduct(pastry.get(position).getId());
            }
        });

        //holder.desc.setText(pastry.get(position).getDesc().getSmalldesc());
       // holder.like.setVisibility(View.GONE);
        holder.nomPastry.setText(pastry.get(position).getNom());
        holder.adresse.setText(pastry.get(position).getAdresse());
        queue = Volley.newRequestQueue(context);



//calculer le nbr d'abonner

        StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlnbrAbonner,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            holder.numAbonner.setText(jsonobject.getInt("somme")+" abonné(s)");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", pastry.get(position).getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAbonner);


//insertion photo de la pastry
        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+pastry.get(position).getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        holder.imagepastry.setImageBitmap(bitmap);
                        holder.spinner.setVisibility(View.GONE);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        holder.imagepastry.setImageResource(R.drawable.logo);
                        holder.spinner.setVisibility(View.GONE);

                    }
                });
// Access the RequestQueue through your singleton class.

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
        holder.imagepastry.setBackgroundResource(R.color.trans);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return pastry.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
       // TextView desc ;
        TextView nomPastry ;
        TextView adresse ;
        ImageView catalogue;
        TextView cataloguetext;
         ProgressBar spinner;


        LinearLayout baground;
         TextView numAbonner;
         //TextView like;
         ImageView imagepastry ;
         //ImageView likeImg ;

        ViewHolder(View itemView) {
            super(itemView);
             //desc = (TextView) itemView.findViewById(R.id.petitDesc);
             nomPastry = (TextView) itemView.findViewById(R.id.nomPastry);
             adresse = (TextView) itemView.findViewById(R.id.adresse);
             catalogue = (ImageView) itemView.findViewById(R.id.catalogueimg);
             cataloguetext = (TextView) itemView.findViewById(R.id.cataloguetext);
             baground=(LinearLayout)itemView.findViewById(R.id.bagroundImage);
            spinner = (ProgressBar)itemView.findViewById(R.id.progressBar1);


          numAbonner = (TextView) itemView.findViewById(R.id.numAbonner);
           // like = (TextView) itemView.findViewById(R.id.like);
              imagepastry = (ImageView)itemView.findViewById(R.id.photo);
             // likeImg = (ImageView)itemView.findViewById(R.id.likeImg);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Pastry getItem(int id) {
        return pastry.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    public void login(){
        Intent intent = new Intent(context,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void getProduct(int id){
        Intent intent = new Intent(context,CatalogueProductAcueilActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",id+"");
        context.startActivity(intent);
    }



    public void getpastry(Pastry pastry){

        Intent intent = new Intent(context,PastryFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        context.startActivity(intent);

    }
}