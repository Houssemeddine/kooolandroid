package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class recycleAccueilProductAdapter extends RecyclerView.Adapter<recycleAccueilProductAdapter.ViewHolder> {

    private List<Produit> produit;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    int resources;
    Context context;

    RequestQueue queue ;

    String url = Ressource.url+"/api/tproduits/";
    String urlAbonnerpastry =Ressource.url+"/abonnerProduct";
    String urldesAbonnerpastry =Ressource.url+"/desabonnerProduct";
    String urlverifAbonner =Ressource.url+"/verifierAbonnerProduct";
    String urlnbrAbonner =Ressource.url+"/SumAabonnerProduit";
    String urlSumAvis =Ressource.url+"/getsumAvisProduct";

    String urlPhotos =Ressource.url+"/api/tphotos";
    // data is passed into the constructor
    recycleAccueilProductAdapter(Context context, List<Produit> animals) {
        this.mInflater = LayoutInflater.from(context);
        this.produit = animals;
        this.context=context;

    }

    // inflates the row layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_accueil_cataloque_horizentale, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        //int color = mViewColors.get(position);
        //String animal = mAnimals.get(position);
        holder.nomPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getpastry(produit.get(position).getIdpastry());
            }
        });

        holder.nom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getproduct(produit.get(position));
            }
        });
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getproduct(produit.get(position));
            }
        });

        queue = Volley.newRequestQueue(context);


        holder.prix.setText(produit.get(position).getPrix() + " DT");
        holder.nom.setText(produit.get(position).getNom() + "");
        holder.nomPastry.setText(produit.get(position).getIdpastry().getNom());

        //calculer le nbr d'abonner

        StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlnbrAbonner,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            holder.numLike.setText(jsonobject.getInt("somme")+" j'aime(s)");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", produit.get(position).getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAbonner);


        //insertion de la photo

        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.get(position).getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        holder.img.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        holder.img.setImageResource(R.drawable.logo);
                    }
                });
// Access the RequestQueue through your singleton class.

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
//insertion photo de la pastry
        StringRequest postRequestimg = new StringRequest(Request.Method.GET, urlPhotos,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Image>>() {
                        }.getType();
                        List<Image> produits = null;
                        try {

                            produits = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt");
                            System.out.println("LIST : " + produits);
                            for ( Image image:produits) {
                                if (image.getIdpastry()!=null){
                                    System.out.println("les image ou id  pastry non null");
                                    System.out.println(image);
                                    if (image.getIdpastry().getId()== produit.get(position).getIdpastry().getId()){
                                        System.out.println("instancier un image");
                                        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+image.getLegende()+".jpg",
                                                new Response.Listener<Bitmap>() {
                                                    @Override
                                                    public void onResponse(Bitmap bitmap) {
                                                        holder.imgPastry.setImageBitmap(bitmap);
                                                        holder.spinner.setVisibility(View.GONE);
                                                    }
                                                }, 0, 0, null,
                                                new Response.ErrorListener() {
                                                    public void onErrorResponse(VolleyError error) {
                                                        holder.imgPastry.setImageResource(R.drawable.logo);
                                                        holder.spinner.setVisibility(View.GONE);

                                                    }
                                                });
// Access the RequestQueue through your singleton class.

                                        request.setRetryPolicy(new RetryPolicy() {
                                            @Override
                                            public int getCurrentTimeout() {
                                                return 50000;
                                            }

                                            @Override
                                            public int getCurrentRetryCount() {
                                                return 50000;
                                            }

                                            @Override
                                            public void retry(VolleyError error) throws VolleyError {

                                            }
                                        });
                                        queue.add(request);
                                    }}

                            }
                        }catch (Exception e){
                            System.out.println("Erreur dans la recuperation du gson a string data");
                            holder. imgPastry.setImageResource(R.drawable.logo);
                            holder.spinner.setVisibility(View.GONE);


                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response d'upload", error.toString());
                        holder.imgPastry.setImageResource(R.drawable.logo);
                        holder.spinner.setVisibility(View.GONE);


                    }
                }
        );
        postRequestimg.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestimg); ImageRequest request2 = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.get(position).getIdpastry().getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        holder.imgPastry.setImageBitmap(bitmap);
                        holder.spinner.setVisibility(View.GONE);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        holder.imgPastry.setImageResource(R.drawable.logo);
                        holder.spinner.setVisibility(View.GONE);

                    }
                });
// Access the RequestQueue through your singleton class.

        request2.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request2);
        StringRequest postRequestnbrAvis = new StringRequest(Request.Method.POST, urlSumAvis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            holder.numAvis.setText(jsonobject.getInt("somme")+" Avis ");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit",produit.get(position).getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAvis.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAvis);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return produit.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView desc;
        TextView prix;
        TextView nom ;
        TextView nomPastry ;
         ImageView img ;
         ImageView imgPastry ;
         TextView numLike ;
         TextView numAvis ;
        ProgressBar spinner;
        ViewHolder(View itemView) {
            super(itemView);
             desc = (TextView) itemView.findViewById(R.id.petitDesc);
             prix = (TextView) itemView.findViewById(R.id.prix);
             nom = (TextView) itemView.findViewById(R.id.nom);
             nomPastry = (TextView) itemView.findViewById(R.id.nomPastry);
              img = (ImageView) itemView.findViewById(R.id.imgproduit);
              imgPastry = (ImageView) itemView.findViewById(R.id.imgPastry);
              numLike = (TextView) itemView.findViewById(R.id.numAbonner);
              numAvis = (TextView) itemView.findViewById(R.id.numAvis);
            spinner = (ProgressBar)itemView.findViewById(R.id.progressBar1);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Produit getItem(int id) {
        return produit.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void login(){
        Intent intent = new Intent(context,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    public void getpastry(Pastry pastry){

        Intent intent = new Intent(context,PastryFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        context.startActivity(intent);

    }
    public void getproduct(Produit pastry){

        Intent intent = new Intent(context,productFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        context.startActivity(intent);

    }

}