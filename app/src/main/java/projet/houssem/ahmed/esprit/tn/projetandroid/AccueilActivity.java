package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.PopulairePastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class AccueilActivity extends AppCompatActivity {

    RequestQueue queue ;
    ListView listView;
    String urlPastry = Ressource.url+"/api/tpastries";
    String urlgetPopulairePastry = Ressource.url+"/getPopulairePastry";
    String urlNewPastry = Ressource.url+"/getNewPastry";
    Button btnplusPastry;
    Button btnconnection;
    EditText searchtext;
    Button BtnProximity;
    Button recament;
    Button BtnDernier;
    SwipeRefreshLayout refresh;
//recycleview pour l'affichage horizentalenmet

    LinearLayoutManager layoutManager;
    RecyclerView myList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        btnconnection=(Button)findViewById(R.id.btnconnection);
        BtnProximity=(Button)findViewById(R.id.BtnProximity);
        BtnDernier=(Button)findViewById(R.id.Btndernier);
        refresh=(SwipeRefreshLayout)findViewById(R.id.refresh);
        BtnDernier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        BtnProximity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ProximityPastryActivity.class);

                startActivity(intent);
            }
        });

        recament =(Button)findViewById(R.id.Btndernier);
        recament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),RecemmentVisiteActivity.class);
                startActivity(intent);
            }
        });
//searchtext.setSelected(false);

        searchtext=(EditText)findViewById(R.id.search);
        searchtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),AllPastryActivity.class);

                startActivity(intent);

            }
        });
        btnconnection.setText("Se connect");
        btnconnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccueilActivity.this,LoginActivity.class);

                startActivity(intent);

            }
        });


        btnplusPastry=(Button)findViewById(R.id.btnplus) ;
        btnplusPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccueilActivity.this,AllPastryActivity.class);

                startActivity(intent);

            }
        });
        listView = (ListView)findViewById(R.id.listPastry);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewPastry();
                getPopulairePastry();
                refresh.setRefreshing(false);
            }
        });
        getNewPastry();
        getPopulairePastry();
        /*Button patId = (Button) findViewById(R.id.tests);
        patId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccueilActivity.this,patisserieActivity.class);
                intent.putExtra("variable1",5);
                startActivity(intent);
            }
        });*/
        //horizental affiche
        layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        myList = (RecyclerView) findViewById(R.id.framehoriz);
        myList.setLayoutManager(layoutManager);
    }
    public void getPastry(){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Pastry>>() {
                        }.getType();
                        List<Pastry> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                        final pastryAcueilAdapter adapter = new pastryAcueilAdapter(  getApplicationContext(), R.layout.item_acueil, pastrys);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }
    public void getNewPastry(){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlNewPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Pastry>>() {
                        }.getType();
                        List<Pastry> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                        recycleAccueilPastryAdapter horizentallistAdapter=
                                new recycleAccueilPastryAdapter(getApplicationContext(),pastrys);
                        myList.setAdapter(horizentallistAdapter);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }
    public void getPopulairePastry(){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlgetPopulairePastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<PopulairePastry>>() {
                        }.getType();
                        List<PopulairePastry> populairePastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt"+populairePastrys);
                        List<Pastry> pastrys= new ArrayList<>();
                        for (PopulairePastry populairePastry1:populairePastrys) {
                            pastrys.add(populairePastry1.getPastry());
                        }
                        pastryAcueilAdapter adapter = new pastryAcueilAdapter(  getApplicationContext(), R.layout.item_acueil, pastrys);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();




                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        );
        queue.add(postRequest);



    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }
}
