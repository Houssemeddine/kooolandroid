package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class GererPastryActivity extends AppCompatActivity {

    TextView granddesc;
    TextView petitdesc;
    TextView phone;
    TextView adresse;

    Button btnCatalogue;
    Button btnEdit;

    ImageView imagepastry;
    RequestQueue queue ;
    String urlPhotos = Ressource.url+"/api/tphotos";
    SwipeRefreshLayout refresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gerer_pastry);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        granddesc = (TextView)findViewById(R.id.grandDesc);
        petitdesc = (TextView)findViewById(R.id.petitDesc);
        phone = (TextView)findViewById(R.id.phone);
        adresse =(TextView)findViewById(R.id.localisation);

        imagepastry=(ImageView)findViewById(R.id.imgPastry);

        btnCatalogue=(Button)findViewById(R.id.addcatalogue);
        btnEdit =(Button)findViewById(R.id.editpastry);
        queue = Volley.newRequestQueue(this);
         refresh=(SwipeRefreshLayout)findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                insertdonner();
                refresh.setRefreshing(false);
            }
        });
        insertdonner();
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GererPastryActivity.this,EditPastryActivity.class);

                startActivity(intent);

            }
        });
        btnCatalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GererPastryActivity.this,CatalogueActivity.class);

                startActivity(intent);

            }
        });

    }

    public void insertdonner(){
        Pastry p = User.userConnect.getPastry();

        granddesc.setText(p.getDesc().getBigdesc()+"");
        petitdesc.setText(p.getDesc().getSmalldesc()+"");
phone.setText(p.getTel()+"");
adresse.setText(p.getAdresse()+"");
        setImagePastry();
        setTitle(p.getNom());
    }
    public void setImagePastry(){

        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPhotos,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Image>>() {
                        }.getType();
                        List<Image> produits = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt");
                        System.out.println("LIST : " + produits);
                        for ( Image image:produits) {
                            if (image.getIdpastry()!=null){
                                System.out.println("les image ou id  pastry non null");
                                System.out.println(image);
                            if (image.getIdpastry().getId()==User.userConnect.getPastry().getId()){
                                System.out.println("instancier un image");
                                instancierImage( image);
                            }}

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                    }
                }
        );
        queue.add(postRequest);
    }

    public  void instancierImage(Image image){
        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+image.getLegende()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imagepastry.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imagepastry.setImageResource(R.drawable.hchicha);
                    }
                });
// Access the RequestQueue through your singleton class.
        queue.add(request);

    }
}
