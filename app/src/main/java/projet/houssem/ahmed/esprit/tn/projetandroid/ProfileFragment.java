package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;

public class
ProfileFragment extends Fragment {
TextView phone;
TextView email;
TextView nom;
TextView localisation;
Button btnPastry;
Button profileedit;
Button favorite;
    SwipeRefreshLayout  refresh;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_profil, container, false);
        email = root.findViewById(R.id.mail);
        phone = root.findViewById(R.id.phone);
        nom = root.findViewById(R.id.nom);
btnPastry = (Button)root.findViewById(R.id.pastry);
        profileedit = (Button)root.findViewById(R.id.profileedit);
        favorite = (Button)root.findViewById(R.id.favorite);

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),FavoritePastryActivity.class);

                startActivity(intent);

            }
        });
        profileedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),EditProfileActivity.class);

                startActivity(intent);

            }
        });


btnPastry.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (User.userConnect.getPastry()==null){

        Intent intent = new Intent(getActivity(),AddPastryActivity.class);

        startActivity(intent);
        }else{
            Intent intent = new Intent(getActivity(),GererPastryActivity.class);

            startActivity(intent);
        }

    }
});

        System.out.println(User.userConnect.getEmail()+"emailll");
        System.out.println(User.userConnect.getTel()+"teeelll");
          refresh=(SwipeRefreshLayout)root.findViewById(R.id.refresh);
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                email.setText(User.userConnect.getEmail().toString());
                phone.setText(User.userConnect.getTel()+"");
                nom.setText(User.userConnect.getNom()+"");
                refresh.setRefreshing(false);
            }
        });
    email.setText(User.userConnect.getEmail().toString());
    phone.setText(User.userConnect.getTel()+"");
        nom.setText(User.userConnect.getNom()+"");

        return root;    }


}
