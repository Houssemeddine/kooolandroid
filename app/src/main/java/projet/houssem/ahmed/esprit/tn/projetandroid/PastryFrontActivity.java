package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Avis;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.MapOverlay;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class PastryFrontActivity extends AppCompatActivity {
private int id;
private String nom;
    TextView granddesc;
    TextView petitdesc;
    TextView phone;
    TextView adresse;
    TextView catalogue;
    TextView prix;
     TextView numAbonner;
     TextView numAvis;
     ImageView likeImg;
     TextView like;
    ImageView imagepastry;
    RequestQueue queue ;
    ListView listView;
    EvaluationAdapter adapter;
Button btnavis;
    List<Avis> avis;


    String urlPhotos = Ressource.url+"/api/tphotos";
    String urlAvis =Ressource.url+"/getAvisPastry";
    String urlPastry =Ressource.url+"/api/tpastries/";
    String urlAbonnerpastry =Ressource.url+"/abonnerPastry";
    String urldesAbonnerpastry =Ressource.url+"/desabonnerPastry";
    String urlnbrAbonner =Ressource.url+"/SumAabonnerPastry";
    String urlSumAvisPastry =Ressource.url+"/getSumAvisPastry";
    String urlverifAbonner =Ressource.url+"/verifierAbonnerPastry";


    Pastry pastry;

//pour la map

    private MapView mapView;
    private MapController mapController;
    Drawable marker;
    MapOverlay mapoverlay;

//looding
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pastry_front);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //get l'id de la pastry
        Intent intent = getIntent();
        String value = intent.getStringExtra("id");
        id= Integer.parseInt(value);
        nom=intent.getStringExtra("nom");
        granddesc = (TextView)findViewById(R.id.grandDesc);
        petitdesc = (TextView)findViewById(R.id.petitDesc);
        phone = (TextView)findViewById(R.id.phone);
        adresse =(TextView)findViewById(R.id.localisation);
        catalogue =(TextView)findViewById(R.id.catalogue);

        imagepastry=(ImageView)findViewById(R.id.imgPastry);
          like = (TextView) findViewById(R.id.like);
        numAbonner = (TextView) findViewById(R.id.numAbonner);
        numAvis = (TextView) findViewById(R.id.avis);
          likeImg = (ImageView)findViewById(R.id.likeImg);
        btnavis=(Button)findViewById(R.id.btnavis);
        listView=(ListView)findViewById(R.id.frame);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);

        btnavis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (User.userConnect == null) {
                    Intent intent = new Intent(PastryFrontActivity.this, LoginActivity.class);

                    startActivity(intent);
                } else{
                    Intent intent = new Intent(PastryFrontActivity.this, RaintingActivity.class);
                intent.putExtra("nom", nom);
                intent.putExtra("Evaluation", "Pastry");

                intent.putExtra("idpastry", id + "");
                startActivity(intent);
            }

            }
        });
        //pour la map

        org.osmdroid.config.Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        mapView = (MapView) this.findViewById(R.id.mapview);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapController =(MapController) this.mapView.getController();
        mapController.setZoom(13);




        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProduct(id);
            }
        });

        queue = Volley.newRequestQueue(this);

        getPastry();

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("clik");
                if (User.userConnect==null){
                    login();

                }else
                {
                    if (like.getText().equals("se désabonner")){
                        desabonnerPastry(pastry.getId());
                        like.setText("s'abonner");
                        likeImg.setImageResource(R.drawable.like);

                    }else {
                        abonner(pastry.getId());
                        like.setText("se désabonner");
                        likeImg.setImageResource(R.drawable.favorite);

                    }
                }

            }
        });

        SumAbonner();
        SumAvis();
        getAvis(id);
        setTitle("Pâtisserie "+intent.getStringExtra("nom"));




    }

    public  void getPastry(){
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry+id,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            System.out.println(jsonobject);
                            Type Typepastry = new TypeToken<Pastry>() {
                            }.getType();
                            pastry = new GsonBuilder().create().fromJson(response.toString(), Typepastry);
                            pastry.setJpsLatitude(jsonobject.getDouble("jps_latitude"));
                            pastry.setJpsLongitude(jsonobject.getDouble("jps_longitude"));
                            System.out.println("hedhia ilpastry"+pastry);
                            VerifAbonner();
                            insertdonner(pastry);

                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;

                            Toast.makeText(context, "Error  de récuperation du donnés  Gsson Builder", duration).show();

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de récuperation du donnés", duration).show();
                    }
                }
        );
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);




    }
    public void insertdonner( Pastry p ){

        granddesc.setText(p.getDesc().getBigdesc()+"");
        petitdesc.setText(p.getDesc().getSmalldesc()+"");
        phone.setText(p.getTel()+"");
        adresse.setText(p.getAdresse()+"");
        System.out.println("title: patiseurie :"+p.getNom());
        setTitle("Pâtisserie:"+p.getNom());
        setImagePastry(p);
        marker=getResources().getDrawable(R.drawable.localisation);
        int markerWidth = marker.getIntrinsicWidth();
        int markerHeight = marker.getIntrinsicHeight();
        marker.setBounds(0, markerHeight, markerWidth, 0);
        mapoverlay = new MapOverlay(getApplicationContext(), marker);
        mapView.getOverlays().add(mapoverlay);
        System.out.println("hedhia il latitude"+p.getJpsLatitude());
        System.out.println("hedhia il longitude"+p.getJpslongitude());
        GeoPoint point = new GeoPoint(p.getJpsLatitude()+0, p.getJpsLongitude()+0);
        mapoverlay.addItem(point, "Russia", "Russia");

        mapController.setCenter(point);
    }
    public void setImagePastry(final Pastry p ){

        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPhotos,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Image>>() {
                        }.getType();
                        List<Image> produits = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt");
                        System.out.println("LIST : " + produits);
                        for ( Image image:produits) {
                            if (image.getIdpastry()!=null){
                                System.out.println("les image ou id  pastry non null");
                                System.out.println(image);
                                if (image.getIdpastry().getId()==p.getId()){
                                    System.out.println("instancier un image");
                                    instancierImage( image);
                                }}

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;
                        spinner.setVisibility(View.GONE);

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                    }
                }
        );
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }

    public  void instancierImage(Image image){
        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+image.getLegende()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
try {
    imagepastry.setImageBitmap(bitmap);
    spinner.setVisibility(View.GONE);
}catch (Exception e){
    System.out.println("on a a null bitmap");
}
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imagepastry.setImageResource(R.drawable.hchicha);
                        spinner.setVisibility(View.GONE);

                    }
                });
// Access the RequestQueue through your singleton class.
        queue.add(request);

    }
    public void login(){
        Intent intent = new Intent(this,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
    }
    public  void abonner(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            if (!jsonobject.getBoolean("abonner")){
                                int duration = Toast.LENGTH_SHORT;


                            }


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);

    }
    public void desabonnerPastry(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urldesAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);


    }

public void SumAbonner(){
    StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlnbrAbonner,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // response
                    Log.d("Response", response.toString());

                    JSONObject jsonobject = null;
                    try {
                        System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                        jsonobject = new JSONObject(response);

                        System.out.println(jsonobject);


                        System.out.println("true subscribe");
                        numAbonner.setText(jsonobject.getInt("somme")+" abonné(s)");



                    } catch (JSONException e) {
                        System.out.println("error dans la response jsonObject");
                        e.printStackTrace();


                    }

                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error
                    System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                    Log.d("Error.Response", error.toString());

                }
            }
    ) {
        @Override
        protected Map<String, String> getParams() {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("Map ");
            Map<String, String> params = new HashMap<String, String>();

            System.out.println("les put");
            params.put("idpastry",id + "");
            System.out.println("fin put ");

            return params;
        }
    };
    postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
        @Override
        public int getCurrentTimeout() {
            return 50000;
        }

        @Override
        public int getCurrentRetryCount() {
            return 50000;
        }

        @Override
        public void retry(VolleyError error) throws VolleyError {

        }
    });
    queue.add(postRequestnbrAbonner);
}
public void SumAvis(){
    StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlSumAvisPastry,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    // response
                    Log.d("Response", response.toString());

                    JSONObject jsonobject = null;
                    try {
                        System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                        jsonobject = new JSONObject(response);

                        System.out.println(jsonobject);


                        System.out.println("true subscribe");
                        numAvis.setText(jsonobject.getInt("somme")+" Avis");



                    } catch (JSONException e) {
                        System.out.println("error dans la response jsonObject");
                        e.printStackTrace();


                    }

                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // error
                    System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                    Log.d("Error.Response", error.toString());

                }
            }
    ) {
        @Override
        protected Map<String, String> getParams() {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("Map ");
            Map<String, String> params = new HashMap<String, String>();

            System.out.println("les put");
            params.put("idpastry",id + "");
            System.out.println("fin put ");

            return params;
        }
    };
    postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
        @Override
        public int getCurrentTimeout() {
            return 50000;
        }

        @Override
        public int getCurrentRetryCount() {
            return 50000;
        }

        @Override
        public void retry(VolleyError error) throws VolleyError {

        }
    });
    queue.add(postRequestnbrAbonner);
}
public  void  VerifAbonner(){

    if (User.userConnect!=null) {
        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlverifAbonner,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);
                            if (jsonobject.getBoolean("subscribe")) {

                                System.out.println("true subscribe");
                                like.setText("se désabonner");
                                likeImg.setImageResource(R.drawable.favorite);

                            }

                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("pastry", pastry.getId() + "");
                params.put("user", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);




    }
}
    public void getProduct(int id){
        Intent intent = new Intent(this,CatalogueProductAcueilActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",id+"");
         startActivity(intent);
    }

    public void getAvis(final int  idpastry){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlAvis,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            Log.d("Response", response.toString());
                            JSONObject jsonobject = null;
                            Type listType = new TypeToken<ArrayList<Avis>>() {
                            }.getType();
                            avis = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt"+avis);


                            adapter = new EvaluationAdapter(  getApplicationContext(), R.layout.item_evaluation, avis);

                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }catch (Exception e){

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idpastry", idpastry + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }

}
