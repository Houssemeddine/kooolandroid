package projet.houssem.ahmed.esprit.tn.projetandroid;

        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ProgressBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.RetryPolicy;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.HashMap;
        import java.util.Map;

        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class EditPasswordActivity extends AppCompatActivity {
    EditText password1;
    EditText password2;
    EditText password3;
    // EditText password;
    // EditText repassword;
    Button btn;
   // Button btnModifierPassword;
    //Button btnf;
    TextView Error;

    //préference
    SharedPreferences pref ;// 0 - for private mode
    private ProgressBar spinner;


    User user=new User();
    RequestQueue queue ;
    String url = Ressource.url+"/api/tusers/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);

       // nom=(EditText)findViewById(R.id.nom);
       // tel=(EditText)findViewById(R.id.tel);
        password1=(EditText)findViewById(R.id.lastpassword);
        password2=(EditText)findViewById(R.id.password);
        password3=(EditText)findViewById(R.id.repassword);
        btn=(Button) findViewById(R.id.btnregister);
        btn.setText("Modifier");

        queue = Volley.newRequestQueue(this);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.setVisibility(View.VISIBLE);
                btn.setEnabled(false);
                if (verifCHampEmty()) {
                    if (verifChampPasswordAcctuel(password1.getText().toString())) {

                        if (verifChampPassword(password2.getText().toString(), password3.getText().toString())) {
                            StringRequest postRequest = new StringRequest(Request.Method.PUT, url + User.userConnect.getId(),
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // response
                                            Log.d("Response", response.toString());

                                            JSONObject jsonobject = null;
                                            try {
                                                jsonobject = new JSONObject(response);
//                                                User.userConnect = new User(jsonobject.getInt("id"), jsonobject.getString("nom")
//                                                        ,jsonobject.getString("password"), jsonobject.getInt("tel"), jsonobject.getString("email"));
                                                // authPreference(true);
                                                User.userConnect.setPassword(jsonobject.getString("password"));
                                                SucessInscription();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                System.out.println("error dans la response jsonObject");
                                                tostMessage("Error de saisie");
                                                spinner.setVisibility(View.GONE);
                                                btn.setEnabled(true);


                                            }

                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            tostMessage("Error de saisie");
                                            spinner.setVisibility(View.GONE);
                                            btn.setEnabled(true);

                                        }
                                    }
                            ) {
                                @Override
                                protected Map<String, String> getParams() {
                                    System.out.println("-----------------------------------------------------------------------");
                                    System.out.println("Map ");
                                    Map<String, String> params = new HashMap<String, String>();
                                    System.out.println("les put");
                                    //params.put("email", user.getEmail());
                                   // params.put("role", "Manager");
                                    //params.put("nom", user.getNom());
                                    params.put("password", password2.getText().toString());
                                   // params.put("tel", user.getTel() + "");
                                    System.out.println("fin put ");

                                    return params;
                                }
                            };
                            postRequest.setRetryPolicy(new RetryPolicy() {
                                @Override
                                public int getCurrentTimeout() {
                                    return 50000;
                                }

                                @Override
                                public int getCurrentRetryCount() {
                                    return 50000;
                                }

                                @Override
                                public void retry(VolleyError error) throws VolleyError {

                                }
                            });
                            queue.add(postRequest);

                        }

                     else {
                            tostMessage("Error de Refaire  le mot de passe");
                            spinner.setVisibility(View.GONE);
                            btn.setEnabled(true);

                    }
                }else {
                        tostMessage("Error de Saisir  le mot de passe actuel");
                        spinner.setVisibility(View.GONE);
                        btn.setEnabled(true);

                    }
                }
                else {
                    tostMessage("Saisir toutes les champs ils sont obligatoire !");
                    spinner.setVisibility(View.GONE);
                    btn.setEnabled(true);

                }
            }
        });

    }

    public boolean verifChampPassword(String str1 ,String  str2){

        return str1.equals(str2);

    }
    public boolean verifChampPasswordAcctuel(String str1){

        return str1.equals(User.userConnect.getPassword());

    }
    public boolean verifCHampEmty(){
        if (password1.getText().toString().equals("")||password2.getText().toString().equals("")
                ||password3.getText().toString().equals("")){
            return false;
        }
        return true;
    }
    public void SucessInscription(){
        finish();


    }

    public void authPreference(boolean a){

        pref = getApplicationContext().getSharedPreferences("mypref", Context.MODE_PRIVATE); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("connexion",a);
        editor.putInt("idUser",User.userConnect.getId());

        editor.commit();


    }
    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
