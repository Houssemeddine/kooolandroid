package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Description;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class AddPastryActivity extends AppCompatActivity {


    EditText nompastry;
    EditText adresse;
    EditText telPastrry;
    EditText petitDesc;
    EditText grandDesc;
    EditText Siret;
    Button btnImage;
    Button btnAjouPastry;
    ImageView imagePastry;
    ImageView imageLocalisation;
    private final  int IMG_Request = 1;
    Bitmap bitmap;
    Description description;
    Pastry pastry;
    Image image;
    private SharedPreferences preferences;
    String urlImageupload = Ressource.url+"/ImagePastry";
    String urlsetPastry =Ressource.url+"/SetPastry";
    String urlDes =Ressource.url+"/api/tdescriptions";
    String urlPastry =Ressource.url+"/api/tpastries";
    String urlImage =Ressource.url+"/api/tphotos";
    RequestQueue queue ;
    private ProgressBar spinner;
boolean ajoutImage = false;
Double legende;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pastry);
    nompastry = (EditText)findViewById(R.id.nomPastry);
    adresse = (EditText)findViewById(R.id.adresse);
    telPastrry = (EditText)findViewById(R.id.telPastry);
    petitDesc = (EditText)findViewById(R.id.petitDesc);
    grandDesc = (EditText)findViewById(R.id.grandDesc);
        Siret = (EditText)findViewById(R.id.Siret);
        imageLocalisation =(ImageView) findViewById(R.id.imageLocalisation);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        preferences =getSharedPreferences("x",Context.MODE_PRIVATE);

legende=Math.random() * ( 999999 - 111111 );
        imageLocalisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddPastryActivity.this,MapActivity.class);

                startActivity(intent);

            }
        });


    btnImage =(Button)findViewById(R.id.buttonUploadImage);


        queue = Volley.newRequestQueue(this);

        btnAjouPastry =(Button)findViewById(R.id.btnAjoutPaastry);
        btnAjouPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifCHampEmty()){
                    if (!preferences.getString("longitude","0.0").equals("0.0")) {
                        if (ajoutImage){
                            if (Siret.getText().toString().length()==14){
                                spinner.setVisibility(View.VISIBLE);
                                btnAjouPastry.setEnabled(false);
                                ajoutDescription();
                            }else
                            {
                                tostMessage("il faut ajouter un siret valide composer de 14 nombre");
                                spinner.setVisibility(View.GONE);
                                btnAjouPastry.setEnabled(true);
                            }

                        }else
                        {

                            tostMessage("il faut ajouter une photo");
                            spinner.setVisibility(View.GONE);
                            btnAjouPastry.setEnabled(true);
                        }


                    }else {
                        tostMessage("il faut entrer une localisation");
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }else
                {
                    tostMessage("Saisir toutes les champs ils sont obligatoire !");
                    spinner.setVisibility(View.GONE);
                    btnAjouPastry.setEnabled(true);
                }


            }
        });
        imagePastry =(ImageView) findViewById(R.id.imageViewPastry);

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selctImageFromGalerie();
ajoutImage=true;
                btnImage.setText("modifier l'image");
            }
        });
    }

private  void selctImageFromGalerie(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMG_Request);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==IMG_Request&&resultCode==RESULT_OK&&data !=null){
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                imagePastry.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void UploadImage(){

        StringRequest postRequest = new StringRequest(Request.Method.POST, urlImageupload,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);

                            System.out.println("les reponse in upload image ");
                            if (jsonobject.getString("password").equals("sucess")){

                                User.userConnect.setPastry(pastry);
                                SuccesajoutPastry();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la respose in upload photos ");
                           ;
                        }



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
               // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("image", imageToString(bitmap));
                params.put("urlphotos", "C:/wamp64/www/ServiceMobile/mobileService/web/upload/");
                params.put("legende",  image.getLegende());
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }
    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte [] imgbyte= byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(imgbyte,Base64.DEFAULT);

    }


    private void ajoutPastry(final String id){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        System.out.println("c'est dans l'entrer");
                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            System.out.println(jsonobject);
                            Type Typepastry = new TypeToken<Pastry>() {
                            }.getType();
                            pastry = new GsonBuilder().create().fromJson(response.toString(), Typepastry);
                            pastry.setJpsLatitude(jsonobject.getDouble("jps_latitude"));
                            pastry.setJpsLongitude(jsonobject.getDouble("jps_longitude"));
                            ajoutImage();

                            instancierPastrytoUser();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                            spinner.setVisibility(View.GONE);
                            btnAjouPastry.setEnabled(true);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("adresse", adresse.getText().toString());
                params.put("nom", nompastry.getText().toString());
                params.put("tel",  telPastrry.getText().toString());
                params.put("iddesc",  description.getId()+"");
                params.put("siret",  Siret.getText().toString()+"");
                params.put("image",  id+"");
                params.put("jpsLongitude",  preferences.getString("longitude","0.0"));
                params.put("jpsLatitude",  preferences.getString("latitude","0.0"));

                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }

    public void ajoutDescription(){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlDes,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {

                            jsonobject = new JSONObject(response);
                            description = new Description(jsonobject.getInt("id"),jsonobject.getString("bigdesc"),jsonobject.getString("smalldesc"));
ajoutPastry(legende+"");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                            spinner.setVisibility(View.GONE);
                            btnAjouPastry.setEnabled(true);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response ajout description", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("bigdesc", grandDesc.getText().toString());
                params.put("smalldesc", petitDesc.getText().toString());
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }
    public void ajoutImage(){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlImage,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            image  = new Image(jsonobject.getInt("id")
                                    ,jsonobject.getString("urlphotos"),jsonobject.getString("legende"),pastry);
//min hina
                            UploadImage();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                            spinner.setVisibility(View.GONE);
                            btnAjouPastry.setEnabled(true);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response ajout description", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("urlphotos", "C:/wamp64/www/ServiceMobile/mobileService/web/upload/");
                params.put("legende",  legende+"");
                params.put("idpastry", pastry.getId()+"");
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }

    public void  SuccesajoutPastry(){
        finish();

    }

    public void instancierPastrytoUser(){

        StringRequest postRequest = new StringRequest(Request.Method.POST, urlsetPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse instancier pastry");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response de instantiate pastry for user", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map instancier to user pastry ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("user", User.userConnect.getId()+"");
                params.put("pastry", pastry.getId()+"");
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }
    public boolean verifCHampEmty(){
        if (nompastry.getText().toString().equals("")||adresse.getText().toString().equals("")
                ||telPastrry.getText().toString().equals("")
                ||petitDesc.getText().toString().equals("")
                ||grandDesc.getText().toString().equals("")
                ||Siret.getText().toString().equals("")){
            return false;
        }
        return true;
    }
    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }


}
