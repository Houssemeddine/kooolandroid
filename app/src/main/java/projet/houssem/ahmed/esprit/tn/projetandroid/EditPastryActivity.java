package projet.houssem.ahmed.esprit.tn.projetandroid;

        import android.content.Context;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Base64;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.ProgressBar;
        import android.widget.Toast;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.RetryPolicy;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;

        import org.json.JSONException;
        import org.json.JSONObject;


        import java.util.HashMap;
        import java.util.Map;

        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Description;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class EditPastryActivity extends AppCompatActivity {


    EditText nompastry;
    EditText adresse;
    EditText telPastrry;
    EditText petitDesc;
    EditText grandDesc;

    Button btnAjouPastry;

    private final  int IMG_Request = 1;
    Description description;
    Pastry pastry;

    String urlDes =Ressource.url+"/api/tdescriptions/";
    String urlPastry =Ressource.url+"/api/tpastries/";
  //  String urlImage =Ressource.url+"/api/tphotos";
    RequestQueue queue ;
    private ProgressBar spinner;
   // boolean ajoutImage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pastry);
        nompastry = (EditText)findViewById(R.id.nomPastry);
        adresse = (EditText)findViewById(R.id.adresse);
        telPastrry = (EditText)findViewById(R.id.telPastry);
        petitDesc = (EditText)findViewById(R.id.petitDesc);
        grandDesc = (EditText)findViewById(R.id.grandDesc);

        spinner = (ProgressBar)findViewById(R.id.progressBar1);

        nompastry.setText(User.userConnect.getPastry().getNom());
        adresse.setText(User.userConnect.getPastry().getAdresse());
        telPastrry.setText(User.userConnect.getPastry().getTel()+"");
        petitDesc.setText(User.userConnect.getPastry().getDesc().getSmalldesc());
        grandDesc.setText(User.userConnect.getPastry().getDesc().getBigdesc());






        queue = Volley.newRequestQueue(this);

        btnAjouPastry =(Button)findViewById(R.id.btnAjoutPaastry);
        btnAjouPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifCHampEmty()){

                                spinner.setVisibility(View.VISIBLE);
                                btnAjouPastry.setEnabled(false);
                                ajoutDescription();





                }else
                {
                    tostMessage("Saisir toutes les champs ils sont obligatoire !");
                    spinner.setVisibility(View.GONE);
                    btnAjouPastry.setEnabled(true);
                }


            }
        });

    }

    private void ajoutPastry(){
        StringRequest postRequest = new StringRequest(Request.Method.PUT, urlPastry+User.userConnect.getPastry().getId(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        System.out.println("c'est dans l'entrer");
                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            User.userConnect.getPastry().setAdresse(pastry.getAdresse());
                            User.userConnect.getPastry().setNom(pastry.getNom());
                            User.userConnect.getPastry().setTel(pastry.getTel());
                            SuccesajoutPastry();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                            spinner.setVisibility(View.GONE);
                            btnAjouPastry.setEnabled(true);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'upload image", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("adresse", adresse.getText().toString());
                params.put("nom", nompastry.getText().toString());
                params.put("tel",  telPastrry.getText().toString());
                params.put("iddesc",  description.getId()+"");

                              System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }

    public void ajoutDescription(){
        StringRequest postRequest = new StringRequest(Request.Method.PUT, urlDes+User.userConnect.getPastry().getDesc().getId(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {

                            jsonobject = new JSONObject(response);
                          //  description = new Description(jsonobject.getInt("id"),jsonobject.getString("bigdesc"),jsonobject.getString("smalldesc"));
                            User.userConnect.getPastry().getDesc().setBigdesc(description.getBigdesc());
                            User.userConnect.getPastry().getDesc().setSmalldesc(description.getSmalldesc());
                            ajoutPastry();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                            spinner.setVisibility(View.GONE);
                            btnAjouPastry.setEnabled(true);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response ajout description", duration).show();
                        spinner.setVisibility(View.GONE);
                        btnAjouPastry.setEnabled(true);
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("bigdesc", grandDesc.getText().toString());
                params.put("smalldesc", petitDesc.getText().toString());
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }

    public void  SuccesajoutPastry(){
        finish();

    }

    public boolean verifCHampEmty(){
        if (nompastry.getText().toString().equals("")||adresse.getText().toString().equals("")
                ||telPastrry.getText().toString().equals("")
                ||petitDesc.getText().toString().equals("")
                ||grandDesc.getText().toString().equals("")){
            return false;
        }
        return true;
    }
    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
