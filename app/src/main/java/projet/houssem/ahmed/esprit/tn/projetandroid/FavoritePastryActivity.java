package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Subscribe;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class FavoritePastryActivity extends AppCompatActivity {


    MaterialSearchView searchView;

    RequestQueue queue ;
    ListView listView;
    String urlPastry = Ressource.url+"/getpastrySubscribe";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_pastry);

        listView = (ListView)findViewById(R.id.listPastry);
        searchView = (MaterialSearchView)findViewById(R.id.search_view);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Vos Favoris");
        getPastry();
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {

                getPastry();

            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null && !newText.isEmpty()){
                    List<String> lstFound = new ArrayList<String>();
                    getPastrysearch(newText);
                }
                else{
                    //if search text is null
                    //return default
                    getPastry();

                }
                return true;
            }

        });

    }

    public void getPastry(){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            Log.d("Response", response.toString());
                            JSONObject jsonobject = null;
                            Type listType = new TypeToken<ArrayList<Subscribe>>() {
                            }.getType();
                            List<Subscribe> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                            List<Pastry>list=new ArrayList<Pastry>();
                            for(Subscribe item:pastrys){
                                list.add(item.getIdpastry());
                            }
                            final pastryAcueilAdapter adapter = new pastryAcueilAdapter(  getApplicationContext(), R.layout.item_acueil, list);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        }
                        catch (Exception e){
                            getPastry();

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");

                params.put("iduser",  User.userConnect.getId()+"");

                System.out.println("fin put ");

                return params;
            }
        };
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }
    public void getPastrysearch(final String newText){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Subscribe>>() {
                        }.getType();
                        List<Subscribe> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                        System.out.println("LIIIIIIIIIIIIIIIISt"+pastrys);

                        List<Pastry>list=new ArrayList<Pastry>();
                        for(Subscribe item:pastrys){
                            list.add(item.getIdpastry());
                        }

                        List<Pastry> lstFound = new ArrayList<Pastry>();
                        for(Pastry item:list){
                            if(item.getNom().contains(newText))
                                lstFound.add(item);
                        }



                        final pastryAcueilAdapter adapter = new pastryAcueilAdapter(  getApplicationContext(), R.layout.item_acueil, lstFound);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de connexion", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");

                params.put("iduser",  User.userConnect.getId()+"");

                System.out.println("fin put ");

                return params;
            }
        };
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }
}
