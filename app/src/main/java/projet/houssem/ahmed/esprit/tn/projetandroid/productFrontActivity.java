package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Avis;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.MapOverlay;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class productFrontActivity extends AppCompatActivity {
    private int id;
   // private String nom;
    TextView nomProduit;
    TextView nomPastry;
    TextView prix;
    TextView granddesc;
    TextView petitdesc;
    TextView adresse;
    TextView numAbonner;
    TextView numAvis;
    ImageView likeImg;
    TextView like;
    ImageView imagepastry;
    ImageView imageproduct;
    RequestQueue queue ;
    ListView listView;
    Button btnavis;
    List<Avis> avis;
    EvaluationAdapter adapter;
    String url = Ressource.url+"/api/tproduits/";
    String urlAbonnerpastry =Ressource.url+"/abonnerProduct";
    String urldesAbonnerpastry =Ressource.url+"/desabonnerProduct";
    String urlverifAbonner =Ressource.url+"/verifierAbonnerProduct";
    String urlnbrAbonner =Ressource.url+"/SumAabonnerProduit";
    String urlPhotos =Ressource.url+"/api/tphotos";
    String urlProduit =Ressource.url+"/api/tproduits/";
    String urlAvis =Ressource.url+"/getAvisProduct";
    String urlSumAvis =Ressource.url+"/getsumAvisProduct";
    Produit produit;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_front);
        Intent intent = getIntent();
        String value = intent.getStringExtra("id");
        id= Integer.parseInt(value);
        nomPastry=(TextView)findViewById(R.id.nomPastry);
        nomProduit=(TextView)findViewById(R.id.nomProduit);
        prix=(TextView)findViewById(R.id.prixProduit);
        granddesc = (TextView)findViewById(R.id.grandDesc);
        petitdesc = (TextView)findViewById(R.id.petitDesc);
        adresse =(TextView)findViewById(R.id.adresse);
        imageproduct=(ImageView)findViewById(R.id.imgproduit);
        imagepastry=(ImageView)findViewById(R.id.imgPastry);
        listView=(ListView)findViewById(R.id.frame);
        numAbonner = (TextView) findViewById(R.id.numAbonner);
        numAvis=(TextView)findViewById(R.id.avis);
        like = (TextView) findViewById(R.id.like);
        likeImg = (ImageView)findViewById(R.id.likeImg);
        btnavis = (Button) findViewById(R.id.btnavis);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);


        queue = Volley.newRequestQueue(this);


        getProduit();

    }
    public void getProduit(){
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlProduit+id,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            System.out.println(jsonobject);
                            Type Typepastry = new TypeToken<Produit>() {
                            }.getType();
                            produit = new GsonBuilder().create().fromJson(response.toString(), Typepastry);

                            System.out.println("hedhia ilproduit"+produit);
                            //VerifAbonner();
                            insertdonner(produit);

                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;

                            Toast.makeText(context, "Error  de récuperation du donnés  Gsson Builder", duration).show();

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  de récuperation du donnés", duration).show();
                    }
                }
        );
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);

    }
    public void insertdonner( final Produit p ){

        granddesc.setText(p.getIddesc().getBigdesc()+"");
        petitdesc.setText(p.getIddesc().getSmalldesc()+"");
        adresse.setText(p.getIdpastry().getAdresse()+"");
        prix.setText(p.getPrix()+"");
        nomProduit.setText(p.getNom()+"");
        nomPastry.setText(" Pâtisserie "+p.getIdpastry().getNom()+"");
        nomPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpastry(p.getIdpastry());
            }
        });
        imagepastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpastry(p.getIdpastry());
            }
        });

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("clik");
                if (User.userConnect==null){
                    login();

                }else
                {
                    if (like.getText().equals("j'aime déjà")){
                        desabonnerPastry(produit.getId());
                        like.setText("j'aime");
                        likeImg.setImageResource(R.drawable.like);

                    }else {
                        abonner(produit.getId());
                        like.setText("j'aime déjà");
                        likeImg.setImageResource(R.drawable.favorite);

                    }
                }

            }
        });
        btnavis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (User.userConnect == null) {
                    Intent intent = new Intent(productFrontActivity.this, LoginActivity.class);

                    startActivity(intent);
                } else{
                    Intent intent = new Intent(productFrontActivity.this, RaintingActivity.class);
                    intent.putExtra("nom", p.getNom());
                    intent.putExtra("Evaluation", "Product");

                    intent.putExtra("idproduit", id + "");
                    startActivity(intent);
                }

            }
        });
        getAvis(id);
        SumAvis();
        calculnbrAbonner();
        verifUserLike();
        insertimgProduit();
        InsertimgPastry();
    }
    public void like(){
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("clik");
                if (User.userConnect==null){
                    login();

                }else
                {
                    if (like.getText().equals("j'aime déjà")){
                        desabonnerPastry(produit.getId());
                        like.setText("j'aime");
                        likeImg.setImageResource(R.drawable.like);

                    }else {
                        abonner(produit.getId());
                        like.setText("j'aime déjà");
                        likeImg.setImageResource(R.drawable.favorite);

                    }
                }

            }
        });
    }
    public void calculnbrAbonner(){
        StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlnbrAbonner,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            numAbonner.setText(jsonobject.getInt("somme")+" j'aime(s)");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", produit.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAbonner);

    }
    public void verifUserLike(){
        if (User.userConnect!=null) {
            StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlverifAbonner,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            Log.d("Response", response.toString());

                            JSONObject jsonobject = null;
                            try {
                                System.out.println("entrer dans la recuperaation des donnes");
                                jsonobject = new JSONObject(response);

                                System.out.println(jsonobject);
                                if (jsonobject.getBoolean("subscribe")) {

                                    System.out.println("true subscribe");
                                    like.setText("j'aime déjà");
                                    likeImg.setImageResource(R.drawable.favorite);

                                }

                            } catch (JSONException e) {
                                System.out.println("error dans la response jsonObject");
                                e.printStackTrace();


                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                            Log.d("Error.Response", error.toString());

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    System.out.println("-----------------------------------------------------------------------");
                    System.out.println("Map ");
                    Map<String, String> params = new HashMap<String, String>();

                    System.out.println("les put");
                    params.put("product", produit.getId() + "");
                    params.put("user", User.userConnect.getId() + "");
                    System.out.println("fin put ");

                    return params;
                }
            };
            postRequestAbonner.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            queue.add(postRequestAbonner);




        }

    }
    public void insertimgProduit(){
        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageproduct.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imageproduct.setImageResource(R.drawable.logo);
                    }
                });
// Access the RequestQueue through your singleton class.

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
    }
    public void InsertimgPastry(){
        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.getIdpastry().getImage()
                +".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imagepastry.setImageBitmap(bitmap);
                        spinner.setVisibility(View.GONE);

                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imagepastry.setImageResource(R.drawable.logo);
                        spinner.setVisibility(View.GONE);

                    }
                });
// Access the RequestQueue through your singleton class.

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);


    }
    public void login(){
        Intent intent = new Intent(this,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       startActivity(intent);
    }
    public void abonner(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            if (!jsonobject.getBoolean("abonner")){
                                int duration = Toast.LENGTH_SHORT;


                                Toast.makeText(getApplicationContext(), "vous l'aimé déjà", duration).show();
                            }


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);

    }
    public void desabonnerPastry(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urldesAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);


    }
    public void getpastry(Pastry pastry){

        Intent intent = new Intent(this,PastryFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
       startActivity(intent);

    }
    public void getAvis(final int  idpastry){
        queue = Volley.newRequestQueue(this);

        // ArrayList<Produit> produits = new ArrayList<>();
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlAvis,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        try {
                            Log.d("Response", response.toString());
                            JSONObject jsonobject = null;
                            Type listType = new TypeToken<ArrayList<Avis>>() {
                            }.getType();
                            avis = new GsonBuilder().create().fromJson(response.toString(), listType);
                            System.out.println("LIIIIIIIIIIIIIIIISt"+avis);


                            adapter = new EvaluationAdapter(  getApplicationContext(), R.layout.item_evaluation, avis);

                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }catch (Exception e){

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", idpastry + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }
    public void SumAvis(){
        StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlSumAvis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            numAvis.setText(jsonobject.getInt("somme")+" Avis");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit",id + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAbonner);
    }
}
