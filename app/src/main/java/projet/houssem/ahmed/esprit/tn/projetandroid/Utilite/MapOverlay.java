package projet.houssem.ahmed.esprit.tn.projetandroid.Utilite;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;


public class MapOverlay extends ItemizedOverlay<OverlayItem> {

    private ArrayList<OverlayItem> overlayItemList = new   ArrayList<OverlayItem>();

    public MapOverlay(Drawable pDefaultMarker, ArrayList<OverlayItem> overlayItemList) {
        super(pDefaultMarker);
        this.overlayItemList = overlayItemList;
    }

    public MapOverlay(Context ctx, Drawable pDefaultMarker) {
        super(ctx, pDefaultMarker);
    }


    public void addItem(GeoPoint p, String title, String snippet){
        OverlayItem newItem = new OverlayItem(title, snippet, p);
        overlayItemList.add(newItem);
        populate();
    }
    @Override
    public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3){
        return false;
    }

    @Override
    protected OverlayItem createItem(int arg0) {
        return overlayItemList.get(arg0);
    }
    @Override
    public int size() {
        return overlayItemList.size();
    }
}