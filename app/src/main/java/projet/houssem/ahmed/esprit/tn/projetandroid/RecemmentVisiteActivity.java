package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.DatabaseManager;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class RecemmentVisiteActivity extends AppCompatActivity {

    DatabaseManager databaseManager;
    RequestQueue queue ;
    ListView listView;
    String urlPastry = Ressource.url+"/api/tpastries";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recemment_visite);
        databaseManager = new DatabaseManager(this);
        ArrayList<Integer> laliste = databaseManager.getLast6();
        listView= (ListView) findViewById(R.id.elLista);
        databaseManager.close();
        getPastrysearch(laliste);

    }
    public void getPastrysearch(final ArrayList<Integer> laliste){
        queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.GET, urlPastry,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonobject = null;
                        Type listType = new TypeToken<ArrayList<Pastry>>() {
                        }.getType();
                        try {
                            List<Pastry> pastrys = new GsonBuilder().create().fromJson(response.toString(), listType);
                            List<Pastry> lstFound = new ArrayList<Pastry>();
                            for(int i : laliste){
                                for(Pastry item:pastrys){
                                    if(item.getId() == i)
                                        lstFound.add(item);
                                }
                            }

                            final pastryAcueilAdapter adapter = new pastryAcueilAdapter(  getApplicationContext(), R.layout.item_acueil, lstFound);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }catch (Exception e){
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_LONG;
                            Toast.makeText(context, "impossible de charger les patisseries ", duration).show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;
                        Toast.makeText(context, "________________________", duration).show();
                    }
                }
        );
        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }

}