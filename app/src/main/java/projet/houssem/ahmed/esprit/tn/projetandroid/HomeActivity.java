package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Description;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class HomeActivity extends AppCompatActivity {
    SharedPreferences pref ;// 0 - for private mode

    String url = Ressource.url+"/api/tusers/";
    RequestQueue queue ;
    private ProgressBar spinner;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        pref = getApplicationContext().getSharedPreferences("mypref", Context.MODE_PRIVATE); // 0 - for private mode

        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);
        if (pref.getBoolean("connexion",false)){
            spinner.setVisibility(View.VISIBLE);

            if (User.userConnect==null){
        recupererUser();
    }else{
        Sucess();
    }


}


        Button plusTard = (Button) findViewById(R.id.BtnPlusTard);
        plusTard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setContentView(R.layout.activity_accueil);
                Intent intent = new Intent(HomeActivity.this,AccueilActivity.class);
                startActivity(intent);
            }
        });
        Button btnMager = (Button) findViewById(R.id.btnMager);
        btnMager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setContentView(R.layout.activity_accueil);
                Intent intent = new Intent(HomeActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        CallbackManager callbackManager  = CallbackManager.Factory.create();
        LoginButton loginfb = (LoginButton) findViewById(R.id.login_button);
        loginfb.setReadPermissions("email");

        // Callback registration
        loginfb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                System.out.println("Sucess connexion fb ");
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException error) {

            }


        });



    }
    public  void recupererUser(){
        queue = Volley.newRequestQueue(this);

        StringRequest postRequest = new StringRequest(Request.Method.GET, url+pref.getInt("idUser",0),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            System.out.println(jsonobject);

                            if (jsonobject.isNull("idpastry")){

                                System.out.println("user qui n'a pas ajouter encore une patisseurie");
                                User.userConnect= new User(jsonobject.getInt("id"),jsonobject.getString("nom")
                                        ,jsonobject.getString("password"),jsonobject.getInt("tel"),jsonobject.getString("email"));
                                Sucess();

                            }else {
                                Description description = new Description(
                                        jsonobject.getJSONObject("idpastry").getJSONObject("iddesc").getInt("id"),
                                        jsonobject.getJSONObject("idpastry").getJSONObject("iddesc").getString("bigdesc"),
                                        jsonobject.getJSONObject("idpastry").getJSONObject("iddesc").getString("smalldesc"));
                                Pastry pastry = new Pastry(
                                        jsonobject.getJSONObject("idpastry").getInt("id"),
                                        jsonobject.getJSONObject("idpastry").getInt("tel")
                                        , description,
                                        jsonobject.getJSONObject("idpastry").getString("adresse")
                                        , jsonobject.getJSONObject("idpastry").getString("nom"),
                                        jsonobject.getJSONObject("idpastry").getDouble("jps_longitude"),jsonobject.getJSONObject("idpastry").getDouble("jps_latitude"));


                                User.userConnect = new User(jsonobject.getInt("id"), jsonobject.getString("nom")
                                        , jsonobject.getString("password"), jsonobject.getInt("tel"), jsonobject.getString("email"), pastry);
                                Sucess();

                            }
                            System.out.println(User.userConnect);

                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();
                            Context context = getApplicationContext();
                            int duration = Toast.LENGTH_SHORT;
                         //   loading(false);

                           // Toast.makeText(context, "Error  d'identification", duration).show();

                            spinner.setVisibility(View.GONE);
                            editor = pref.edit();
                            editor.putBoolean("connexion",false);
                            //editor.putInt("idUser",User.userConnect.getId());
                            editor.commit();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;
                      //  loading(false);

                     //   Toast.makeText(context, "Error  d'identification", duration).show();
                        spinner.setVisibility(View.GONE);
                        editor = pref.edit();
                        editor.putBoolean("connexion",false);
                        //editor.putInt("idUser",User.userConnect.getId());
                        editor.commit();
                    }
                }
        );
        queue.add(postRequest);

    }
    public void Sucess(){
        spinner.setVisibility(View.GONE);

        Intent intent = new Intent(HomeActivity.this,MainActivity.class);

        startActivity(intent);
    }
}
