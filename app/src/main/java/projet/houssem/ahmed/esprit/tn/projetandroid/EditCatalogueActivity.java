package projet.houssem.ahmed.esprit.tn.projetandroid;

        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.net.Uri;
        import android.provider.MediaStore;
        import android.support.annotation.Nullable;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Base64;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.ProgressBar;
        import android.widget.ScrollView;
        import android.widget.Spinner;
        import android.widget.Toast;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.RetryPolicy;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.ImageRequest;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;
        import com.facebook.internal.Utility;
        import com.google.gson.GsonBuilder;
        import com.google.gson.reflect.TypeToken;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.ByteArrayOutputStream;
        import java.io.IOException;
        import java.lang.reflect.Type;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Description;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
        import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class EditCatalogueActivity extends AppCompatActivity {

    EditText petitDesc;
    EditText grandDesc;
    EditText NomProduit;
    EditText Prix;

    Button btnImage;
    Button btnAjouPastry;
    ImageView imageProduit;
    RequestQueue queue ;

    private Spinner typeProduit;
    boolean ajoutImage = false;


    private ProgressBar spinner;
    ScrollView scrollView ;


    private final  int IMG_Request = 1;
    Bitmap bitmap;
    Description description;
    Image image;
    Produit produit;

    String urlDes = Ressource.url+"/api/tdescriptions/";
    String urlImageupload =Ressource.url+"/ImagePastry";

    String urlImage =Ressource.url+"/api/tphotos";
    String urlProduit =Ressource.url+"/api/tproduits/";
    String idproduit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_catalogue);

        queue = Volley.newRequestQueue(this);
        petitDesc = (EditText)findViewById(R.id.petitDesc);
        grandDesc = (EditText)findViewById(R.id.grandDesc);
        NomProduit = (EditText)findViewById(R.id.nomProduit);
        Prix = (EditText)findViewById(R.id.prix);

        typeProduit=(Spinner)findViewById(R.id.typeproduit);
        typeProduit.setVisibility(View.GONE);
        scrollView=(ScrollView)findViewById(R.id.contenue);

        imageProduit=(ImageView) findViewById(R.id.imageViewProduit);
        btnAjouPastry=(Button)findViewById(R.id.btnAjoutProduit);

        btnAjouPastry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (verifCHampEmty()){

                        spinner.setVisibility(View.VISIBLE);
                        scrollView.setEnabled(false);
                        btnAjouPastry.setEnabled(false);
                        petitDesc.setEnabled(false);
                        grandDesc.setEnabled(false);
                        NomProduit.setEnabled(false);
                        Prix.setEnabled(false);
                        ajoutDescription();

                }else {
                    tostMessage("Saisir toutes les champs ils sont obligatoire !");
                    spinner.setVisibility(View.GONE);
                    scrollView.setEnabled(true);
                    btnAjouPastry.setEnabled(true);
                    petitDesc.setEnabled(true);
                    grandDesc.setEnabled(true);
                    NomProduit.setEnabled(true);
                    Prix.setEnabled(true);
                }
            }

        });
        btnImage =(Button)findViewById(R.id.buttonUploadImageProduit);
        btnImage.setText("modifier l'image");
        btnAjouPastry.setText("modifier");

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selctImageFromGalerie();
                ajoutImage=true;

            }
        });


        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);
        insertdonner(Ressource.produit);
    }



    public void ajoutDescription(){
        StringRequest postRequest = new StringRequest(Request.Method.PUT, urlDes+Ressource.produit.getIddesc().getId(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {

                            jsonobject = new JSONObject(response);
                            description = new Description(jsonobject.getInt("id"),jsonobject.getString("bigdesc"),jsonobject.getString("smalldesc"));
                            ajoutProduit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response ajout description", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("bigdesc", grandDesc.getText().toString());
                params.put("smalldesc", petitDesc.getText().toString());
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }
    private  void selctImageFromGalerie(){

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMG_Request);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==IMG_Request&&resultCode==RESULT_OK&&data !=null){
            Uri path = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                imageProduit.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void UploadImage(final String legende){

        StringRequest postRequest = new StringRequest(Request.Method.POST, urlImageupload,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());
                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);

                            System.out.println("les reponse in upload image ");
                            if (jsonobject.getString("password").equals("sucess")){

                                //User.userConnect.setPastry(pastry);
                                SuccesajoutProduit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la respose in upload photos ");
                        }



                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;
                        SuccesajoutProduit();
                        Toast.makeText(context, "Error  d'upload image", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("image", imageToString(bitmap));
                params.put("urlphotos", "C:/wamp64/www/ServiceMobile/mobileService/web/upload/");
                params.put("legende",  legende+"");
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);



    }
    private String imageToString(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte [] imgbyte= byteArrayOutputStream.toByteArray();

        return Base64.encodeToString(imgbyte,Base64.DEFAULT);

    }
    public void ajoutImage(final String img){
        StringRequest postRequest = new StringRequest(Request.Method.POST, urlImage,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            image  = new Image(jsonobject.getInt("id")
                                    ,jsonobject.getString("urlphotos"),jsonobject.getString("legende"),produit);

                            UploadImage(img);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                            UploadImage(img);

                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'response ajout image", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("urlphotos", "C:/wamp64/www/ServiceMobile/mobileService/web/upload/");
                params.put("legende",  img+"");
               params.put("idproduit", produit.getId()+"");
                System.out.println("fin put ");

                return params;
            }
        };

        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);


    }


    public void ajoutProduit(){
        StringRequest postRequest = new StringRequest(Request.Method.PUT, urlProduit+Ressource.produit.getId(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            produit = new Produit(jsonobject.getInt("id"), jsonobject.getInt("prix"),jsonobject.getString("nom"),description, User.userConnect.getPastry());
                            if (ajoutImage){
                                Double legende=Math.random() * ( 999999 - 111111 );
                                ajoutImage(legende+"");
                                produit.setImage(legende+"");
                                instancierimagetoproduit(legende+"");
                            }else {
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "Error  d'ajout produit", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("prix", Prix.getText().toString()+"");
                params.put("nom", NomProduit.getText().toString());
               // params.put("type", typeProduit.getSelectedItem().toString());
                params.put("iddesc",  description.getId()+"");
                params.put("idpastry",  User.userConnect.getPastry().getId()+"");

                System.out.println("fin put ");

                return params;
            }
        };


        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }
    public void instancierimagetoproduit(final String img){
        StringRequest postRequest = new StringRequest(Request.Method.PUT, urlProduit+Ressource.produit.getId(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            jsonobject = new JSONObject(response);
                            produit = new Produit(jsonobject.getInt("id"), jsonobject.getInt("prix"),jsonobject.getString("nom"),description, User.userConnect.getPastry());


                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("error dans la response jsonObject");
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());
                        Context context = getApplicationContext();
                        int duration = Toast.LENGTH_SHORT;

                      //  Toast.makeText(context, "Error  d'ajout produit", duration).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams()
            {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                // System.out.println("hedhi il image :"+imageToString(bitmap));
                params.put("image", img+"");


                System.out.println("fin put ");

                return params;
            }
        };


        postRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequest);
    }

    public void SuccesajoutProduit(){
        finish();

    }
    public boolean verifCHampEmty(){
        if (NomProduit.getText().toString().equals("")||Prix.getText().toString().equals("")
                ||petitDesc.getText().toString().equals("")
                ||grandDesc.getText().toString().equals("")
                ){
            return false;
        }
        return true;
    }
    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }



    public void insertdonner(final Produit produit){
        NomProduit.setText(produit.getNom());
        Prix.setText(produit.getPrix()+"");
        petitDesc.setText(produit.getIddesc().getSmalldesc()+"");
        grandDesc.setText(produit.getIddesc().getBigdesc()+"");
        //insertion de la photo

        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageProduit.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        //imageProduit.setImageResource(R.drawable.logo);
                    }
                });
// Access the RequestQueue through your singleton class.

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
    }

}
