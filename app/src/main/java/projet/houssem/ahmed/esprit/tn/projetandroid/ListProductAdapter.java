package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Pastry;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.User;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class ListProductAdapter extends ArrayAdapter<Produit> {


    Context context;
    List<Produit> produit;
    int resources;
    RequestQueue queue ;

    String url = Ressource.url+"/api/tproduits/";
    String urlAbonnerpastry =Ressource.url+"/abonnerProduct";
    String urldesAbonnerpastry =Ressource.url+"/desabonnerProduct";
    String urlverifAbonner =Ressource.url+"/verifierAbonnerProduct";
    String urlnbrAbonner =Ressource.url+"/SumAabonnerProduit";

    String urlPhotos =Ressource.url+"/api/tphotos";
    String urlSumAvis =Ressource.url+"/getsumAvisProduct";


    public ListProductAdapter(@NonNull Context context, int resource, @NonNull List<Produit> objects) {
        super(context, resource, objects);
        this.context=context;
        this.produit = objects;
        this.resources=resource;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resources, null);
        final ProgressBar spinner = (ProgressBar)convertView.findViewById(R.id.progressBar1);

        TextView desc = (TextView) convertView.findViewById(R.id.petitDesc);
        TextView prix = (TextView) convertView.findViewById(R.id.prix);
        TextView nom = (TextView) convertView.findViewById(R.id.nom);
        TextView nomPastry = (TextView) convertView.findViewById(R.id.nomPastry);
        final ImageView img = (ImageView) convertView.findViewById(R.id.imgproduit);
        final ImageView imgPastry = (ImageView) convertView.findViewById(R.id.imgPastry);
        final TextView numLike = (TextView) convertView.findViewById(R.id.numAbonner);
        final TextView numAvis = (TextView) convertView.findViewById(R.id.numAvis);
        final TextView like = (TextView) convertView.findViewById(R.id.like);
        final ImageView likeImg = (ImageView)convertView.findViewById(R.id.likeImg);
        System.out.println("hadhaayaaa il produit " + produit.toString());
        desc.setText(produit.get(position).getIddesc().getSmalldesc() + "");
        prix.setText(produit.get(position).getPrix() + " DT");
        nom.setText(produit.get(position).getNom() + "");
        nomPastry.setText(produit.get(position).getIdpastry().getNom());
       imgPastry.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               getpastry(produit.get(position).getIdpastry());

           }
       });
        nomPastry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getpastry(produit.get(position).getIdpastry());
            }
        });

        nom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getproduct(produit.get(position));
            }
        });
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getproduct(produit.get(position));
            }
        });

        queue = Volley.newRequestQueue(context);

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("clik");
                if (User.userConnect==null){
                    login();

                }else
                {
                    if (like.getText().equals("j'aime déjà")){
                        desabonnerPastry(produit.get(position).getId());
                        like.setText("j'aime");
                        likeImg.setImageResource(R.drawable.like);

                    }else {
                        abonner(produit.get(position).getId());
                        like.setText("j'aime déjà");
                        likeImg.setImageResource(R.drawable.favorite);

                    }
                }

            }
        });

        //calculer le nbr d'abonner

        StringRequest postRequestnbrAbonner = new StringRequest(Request.Method.POST, urlnbrAbonner,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            numLike.setText(jsonobject.getInt("somme")+" j'aime(s)");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", produit.get(position).getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAbonner);

        //voir si l'utilisateurs connecter est abonner cette pastry ou non
        if (User.userConnect!=null) {
            StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlverifAbonner,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response
                            Log.d("Response", response.toString());

                            JSONObject jsonobject = null;
                            try {
                                System.out.println("entrer dans la recuperaation des donnes");
                                jsonobject = new JSONObject(response);

                                System.out.println(jsonobject);
                                if (jsonobject.getBoolean("subscribe")) {

                                    System.out.println("true subscribe");
                                    like.setText("j'aime déjà");
                                    likeImg.setImageResource(R.drawable.favorite);

                                }

                            } catch (JSONException e) {
                                System.out.println("error dans la response jsonObject");
                                e.printStackTrace();


                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                            Log.d("Error.Response", error.toString());

                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    System.out.println("-----------------------------------------------------------------------");
                    System.out.println("Map ");
                    Map<String, String> params = new HashMap<String, String>();

                    System.out.println("les put");
                    params.put("product", produit.get(position).getId() + "");
                    params.put("user", User.userConnect.getId() + "");
                    System.out.println("fin put ");

                    return params;
                }
            };
            postRequestAbonner.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            queue.add(postRequestAbonner);




        }



   //insertion de la photo

        ImageRequest request = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.get(position)
                .getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        img.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        img.setImageResource(R.drawable.logo);
                    }
                });
// Access the RequestQueue through your singleton class.

        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request);
//insertion photo de la pastry
        ImageRequest request2 = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.get(position).
                getIdpastry().getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imgPastry.setImageBitmap(bitmap);
                        spinner.setVisibility(View.GONE);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imgPastry.setImageResource(R.drawable.logo);
                        spinner.setVisibility(View.GONE);
                    }
                });
// Access the RequestQueue through your singleton class.

        request2.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request2);

//calcule nbre avis

        StringRequest postRequestnbrAvis = new StringRequest(Request.Method.POST, urlSumAvis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes du nobre d'abonner ");
                            jsonobject = new JSONObject(response);

                            System.out.println(jsonobject);


                            System.out.println("true subscribe");
                            numAvis.setText(jsonobject.getInt("somme")+" Avis ");



                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse verif aboer pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit",produit.get(position).getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestnbrAvis.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestnbrAvis);

        return convertView;
    }

    public void login(){
        Intent intent = new Intent(getContext(),LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    public  void abonner(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urlAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);
                            if (!jsonobject.getBoolean("abonner")){
                                int duration = Toast.LENGTH_SHORT;


                                Toast.makeText(context, "vous l'aimé déjà", duration).show();
                            }


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);

    }
    public void desabonnerPastry(final int idpastry){

        StringRequest postRequestAbonner = new StringRequest(Request.Method.POST, urldesAbonnerpastry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la recuperaation des donnes");
                            jsonobject = new JSONObject(response);


                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse abonner  pastry");
                        Log.d("Error.Response", error.toString());

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                System.out.println("-----------------------------------------------------------------------");
                System.out.println("Map ");
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("les put");
                params.put("idproduit", idpastry  + "");
                params.put("iduser", User.userConnect.getId() + "");
                System.out.println("fin put ");

                return params;
            }
        };
        postRequestAbonner.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(postRequestAbonner);


    }
    public void getpastry(Pastry pastry){

        Intent intent = new Intent(getContext(),PastryFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        getContext().startActivity(intent);

    }
    public void getproduct(Produit pastry){

        Intent intent = new Intent(getContext(),productFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        getContext().startActivity(intent);

    }
}
