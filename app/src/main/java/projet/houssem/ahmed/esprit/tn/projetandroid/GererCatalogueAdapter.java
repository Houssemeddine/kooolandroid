package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Image;
import projet.houssem.ahmed.esprit.tn.projetandroid.Entity.Produit;
import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.Ressource;

public class GererCatalogueAdapter extends ArrayAdapter<Produit> {
    Context context;
    List<Produit> produit;
    int resources;
    RequestQueue queue ;
    Response.Listener<String> listener;
    String url = Ressource.url+"/api/tproduits/";
    String urlPhotos =Ressource.url+"/api/tphotos";

    public GererCatalogueAdapter(@NonNull Context context, int resource, @NonNull List<Produit> objects) {
        super(context, resource, objects);
        this.context=context;
        this.produit = objects;
        this.resources=resource;

    }

    @NonNull
    @Override
    public Context getContext() {
        return super.getContext();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater= LayoutInflater.from(context);
        convertView = inflater.inflate(resources, null);

        TextView desc = (TextView) convertView.findViewById(R.id.petitDesc);
        TextView prix = (TextView) convertView.findViewById(R.id.prix);
        TextView supprimer = (TextView) convertView.findViewById(R.id.supprimer);
        TextView nom = (TextView) convertView.findViewById(R.id.nom);
        TextView edit = (TextView) convertView.findViewById(R.id.edit);
      final   ImageView img = (ImageView) convertView.findViewById(R.id.imgproduit);
        Button btnPlus =(Button)convertView.findViewById(R.id.btnplus);
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getproduct(produit.get(position));
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ressource.produit=produit.get(position);
                Intent intent = new Intent(context,EditCatalogueActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        System.out.println("hadhaayaaa il produit "+produit.toString());
        desc.setText(produit.get(position).getIddesc().getSmalldesc()+"");
        prix.setText(produit.get(position).getPrix()+" DT");
        nom.setText(produit.get(position).getNom()+"");
        queue = Volley.newRequestQueue(context);

        supprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /* AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want delete this?");

                // Setting Icon to Dialog
                //alertDialog.setIcon(R.drawable.delete);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {

                        // Write your code here to invoke YES event
                        //Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                        supprimerProduit(produit.get(position));
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                       // Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();*/

                supprimerProduit(produit.get(position));

            }
        });
        //insertion de la photo

        ImageRequest request2 = new ImageRequest(Ressource.urlImage+"/ServiceMobile/mobileService/web/upload/"+produit.get(position).getImage()+".jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        img.setImageBitmap(bitmap);
                        //spinner.setVisibility(View.GONE);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        img.setImageResource(R.drawable.logo);
                       // spinner.setVisibility(View.GONE);
                    }
                });
// Access the RequestQueue through your singleton class.

        request2.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(request2);



        return convertView;
    }
    public void supprimerProduit(Produit produit){

        StringRequest postRequest = new StringRequest(Request.Method.DELETE, url+produit.getId(),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response.toString());

                        JSONObject jsonobject = null;
                        try {
                            System.out.println("entrer dans la supprision de produit ");
                            jsonobject = new JSONObject(response);
                            System.out.println(jsonobject);
                            int duration = Toast.LENGTH_SHORT;

                            Toast.makeText(context, "le produit est supprimer charger la page ", duration).show();

                        } catch (JSONException e) {
                            System.out.println("error dans la response jsonObject");
                            e.printStackTrace();
                            int duration = Toast.LENGTH_SHORT;

                            //Toast.makeText(context, "entrer dans la supprision de produit1", duration).show();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        System.out.println("error dans la methode response onErrorResponse");
                        Log.d("Error.Response", error.toString());

                        int duration = Toast.LENGTH_SHORT;

                        Toast.makeText(context, "le produit est supprimer charger la page ", duration).show();
                    }
                }
        );
        queue.add(postRequest);


    }
    public void getproduct(Produit pastry){

        Intent intent = new Intent(getContext(),productFrontActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id",pastry.getId()+"");
        intent.putExtra("nom",pastry.getNom()+"");
        getContext().startActivity(intent);

    }

}
