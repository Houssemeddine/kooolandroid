package projet.houssem.ahmed.esprit.tn.projetandroid;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import projet.houssem.ahmed.esprit.tn.projetandroid.Utilite.MapOverlay;


public class MapActivity extends Activity {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    Button btnLocalisation;
    Drawable marker;
    MapOverlay mapoverlay;
Double latitude;
Double longitude;
    ArrayList<OverlayItem> overlayItemArray;
    private MapView mapView;
    private MapController mapController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        btnLocalisation=(Button)findViewById(R.id.btnLocalisation);


        org.osmdroid.config.Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        mapView = (MapView) this.findViewById(R.id.mapview);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapController =(MapController) this.mapView.getController();
        mapController.setZoom(13);
        mapController.setCenter(new GeoPoint(36.766186,10.100101));

        btnLocalisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences = getSharedPreferences("x", Context.MODE_PRIVATE);
                editor = preferences.edit();
                editor.putString("latitude",latitude+"");
                editor.putString("longitude",longitude+"");
                editor.commit();
                MapActivity.super.onBackPressed();
            }
        });
/*mapView.setLongClickable(true);
mapView.setMultiTouchControls(true);
mapView.setSelected(true);
        Toast.makeText(this,mapView.getLatitudeSpanDouble()+"",Toast.LENGTH_SHORT).show();*/


        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {

//                marker.setVisible(false,true);
                mapView.getOverlays().remove( mapoverlay);

                 mapoverlay = null;
                 marker=getResources().getDrawable(R.drawable.localisation);
                int markerWidth = marker.getIntrinsicWidth();
                int markerHeight = marker.getIntrinsicHeight();
                marker.setBounds(0, markerHeight, markerWidth, 0);

               // ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
                mapoverlay = new MapOverlay(getApplicationContext(), marker);
                mapView.getOverlays().add(mapoverlay);
                //marker.setVisible(true,true);
                latitude=p.getLatitude();
                longitude=p.getLongitude();
                GeoPoint point = new GeoPoint(p.getLatitude(), p.getLongitude());
                mapoverlay.addItem(point, "Russia", "Russia");

               mapController.setCenter(point);
             //   mapController.zoomToSpan(p.getLatitude(), p.getLongitude());

             //   Toast.makeText(getBaseContext(),p.getLatitude() + " - "+p.getLongitude(),Toast.LENGTH_LONG).show();
                btnLocalisation.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {
                return false;
            }
        };


        MapEventsOverlay OverlayEvents = new MapEventsOverlay(getBaseContext(), mReceive);
        mapView.getOverlays().add(OverlayEvents);

    }
    public void tostMessage(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
