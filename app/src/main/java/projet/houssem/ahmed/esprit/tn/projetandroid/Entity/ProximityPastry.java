package projet.houssem.ahmed.esprit.tn.projetandroid.Entity;

public class ProximityPastry {

    private Pastry pastry;
    private Double distance;

    public Pastry getPastry() {
        return pastry;
    }

    public void setPastry(Pastry pastry) {
        this.pastry = pastry;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "ProximityPastry{" +
                "pastry=" + pastry +
                ", distance=" + distance +
                '}';
    }

    public ProximityPastry(Pastry pastry, Double distance) {
        this.pastry = pastry;
        this.distance = distance;
    }

    public ProximityPastry() {
    }
}
